package iusvyryd.courses.kpi.week4;

import iusvyryd.courses.kpi.DataLoader;

import java.io.IOException;
import java.util.Arrays;

/**
 * Уявіть, що ви працюєте у технологічній компанії, яка має захищену лабораторію з
 * надсучасним обладнанням. Доступ до лабораторії здійснюється за паролем. Одного дня ви
 * забули пароль і маєте його відновити. Ви не можете розраховувати на багато спроб, адже
 * після кожної невдалої спроби вводу пароля двері в лабораторії блокуються на один день.
 *
 * Втім, вам відомий алгоритм генерування паролю. Для нього використовується база, яка
 * складається з 1000 трьохсимвольних рядків у випадковому порядку. Пароль має наступний
 * формат.
 *
 * Довжина паролю - 7 літер
 * Перші три літери - це символьний рядок, який йде першим у лексикографічному порядку
 * в базі рядків
 * Четверта літера - це літера, яка трапляється найчастіше серед усіх рядків
 * Останні три літери - це символьний рядок, який йде останнім у лексикографічному порядку
 * в базі рядків
 * Ваша задача полягає у розгадуванні паролю.
 *
 * ПОПЕРЕДНІ ДОМОВЛЕНОСТІ
 *
 * В практичних завданнях нижче ви повинні враховувати наступні попередні домовленості.
 * В символьних рядках використовується англійський алфавіт, всі символи в нижньому
 * регістрі: abcdefghijklmnopqrstuvwxyz.
 * Якщо декілька літер мають максимальну кількість входжень в базу рядків, то тоді
 * відповідно існує декілька варіацій паролю.
 *
 * 1)
 * Ви повинні реалізувати алгоритм сортування за розрядами (radix sort)
 * для розв’язку цієї задачі. В якості внутрішнього алгоритму,
 * який викликається для сортування за кожним окремим розрядом,
 * повинен використовуватись метод сортування підрахунком (counting sort).
 * Введіть вірний пароль, який розблокує двері лабораторії.
 *
 * 2)
 * Перевіримо тепер роботу вашого алгоритму. Використовуючи сортування за розрядами
 * (radix sort), ви повинні робити три виклики внутрішнього алгоритму, який сортує
 * рядки за розрядами, починаючи з молодшого. Зверніть увагу, ваш внутрішній алгоритм
 * повинен задовольняти властивості стійкості сортування.
 * Введіть символьний рядок (складається з трьох літер), який буде першим в базі
 * після першої ітерації алгоритму сортування за розрядами (тобто після сортування за
 * молодшим розрядом).
 * Введіть символьний рядок, який буде першим в базі після другої ітерації алгоритму
 * сортування за розрядами.
 */
public class PracticalTask4 {

    private final int shift = Integer.valueOf('a');
    private final int length = 26;
    private int[] charsTotalCount = new int[length];

    private char getFrequentlyOccursChar() {
        int freqIdx = 0;
        for (int i = 1; i < charsTotalCount.length; i++) {
            if (charsTotalCount[freqIdx] < charsTotalCount[i]) {
                freqIdx = i;
            }
        }
        return Character.toChars(freqIdx + shift)[0];
    }

    public String[] sort(String[] db) {
        int maxLength = db[0].length();
        for (int i = 1; i < db.length; i++) {
            if (db[i].length() > maxLength) {
                maxLength = db[i].length();
            }
        }

        for (int i = 0, pos = maxLength - 1; i < maxLength; i++, pos--) {
            char[] chars = new char[db.length];
            for (int j = 0; j < db.length; j++) {
                try {
                    chars[j] = db[j].charAt(pos);
                }
                catch (IndexOutOfBoundsException e) {
                }
            }

            // counting sort
            int[] temp = new int[length];
            for (int j = 0; j < chars.length; j++) {
                temp[chars[j] - shift]++;
                charsTotalCount[chars[j] - shift]++;
            }
            for (int j = 1; j < temp.length; j++) {
                temp[j] = temp[j] + temp[j - 1];
            }

            String[] result = new String[db.length];
            for (int j = chars.length - 1; j >= 0; j--) {
                result[temp[chars[j] - shift] - 1] = db[j];
                --temp[chars[j] - shift];
            }
            System.arraycopy(result, 0, db, 0, result.length);

            System.out.print("iteration #" + i + ":");
            System.out.println(Arrays.toString(db));
        }
        return db;
    }

    public static void main(String[] args) throws IOException {
        String[] db = DataLoader.loadStringArrayWithUnknownSize("week4/input_db.txt");

        PracticalTask4 p = new PracticalTask4();
        p.sort(db);
        System.out.println("Password is: " +
                db[0] + p.getFrequentlyOccursChar() + db[db.length - 1]);
    }
}
