package iusvyryd.courses.kpi.week4;

import iusvyryd.courses.kpi.AbstractSortingAlgorithm;
import iusvyryd.courses.kpi.AlgorithmUtils;
import iusvyryd.courses.kpi.week3.OrderStatisticsUtils;

/**
 * Radix sort algorithm implementation
 * Non-comparison
 */
public class RadixSort extends AbstractSortingAlgorithm<Integer> {

    public RadixSort() {
        super();
    }

    public RadixSort(Direction direction) {
        super(direction);
    }

    private int digitCount(int num) {
        int cnt = 0;
        while (num > 0) {
            num /= 10;
            cnt++;
        }
        return cnt;
    }

    @Override
    public Integer[] sort(Integer[] data) {
        if (data == null) {
            return null;
        }

        AlgorithmUtils.Pair<Integer, Integer> minMax = OrderStatisticsUtils.minAndMax(data);
        int digitCnt = digitCount(minMax.second());

        for (int i = 0, pl = 1; i < digitCnt; i++, pl *= 10) {
            Integer[] digits = new Integer[data.length];
            for (int j = 0; j < data.length; j++) {
                digits[j] = data[j] / pl % 10;
            }

            // counting sort
            int[] temp = new int[10];
            for (int j = 0; j < digits.length; j++) {
                temp[digits[j]]++;
            }
            for (int j = 1; j < temp.length; j++) {
                temp[j] = temp[j] + temp[j - 1];
            }
            Integer[] result = new Integer[data.length];
            if (direction == Direction.DESC) {
                for (int j = 0; j < digits.length; j++) {
                    result[result.length - temp[digits[j]]] = data[j];
                    --temp[digits[j]];
                }
            }
            else {
                for (int j = digits.length - 1; j >= 0; j--) {
                    result[temp[digits[j]] - 1] = data[j];
                    --temp[digits[j]];
                }
            }
            System.arraycopy(result, 0, data, 0, data.length);
        }
        return data;
    }
}
