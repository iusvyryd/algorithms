package iusvyryd.courses.kpi.week4;

import iusvyryd.courses.kpi.AbstractSortingAlgorithm;
import iusvyryd.courses.kpi.AlgorithmUtils;
import iusvyryd.courses.kpi.week3.OrderStatisticsUtils;

/**
 * Counting sort algorithm implementation
 * Non-comparison
 *
 * Time complexity:
 * average - n + k (if k = O(n) => n)
 *
 * additional memory -  n + k
 * stable - true
 */
public class CountingSort extends AbstractSortingAlgorithm<Integer> {

    public CountingSort() {
        super();
    }

    public CountingSort(Direction direction) {
        super(direction);
    }

    @Override
    public Integer[] sort(Integer[] data) {
        if (data == null) {
            return null;
        }

        AlgorithmUtils.Pair<Integer, Integer> minMax = OrderStatisticsUtils.minAndMax(data);
        return sort(data, minMax.first(), minMax.second());
    }

    private Integer[] sort(Integer[] data, int min, int max) {
        Integer[] temp = new Integer[max - min + 1];
        for (int i = 0; i < temp.length; i++) {
            temp[i] = 0;
        }
        for (int i = 0; i < data.length; i++) {
            temp[data[i] - min]++;
        }
        for (int i = 1; i < temp.length; i++) {
            temp[i] = temp[i] + temp[i - 1];
        }

        Integer[] result = new Integer[data.length];
        for (int i = data.length - 1; i >= 0; i--) {
            if (direction == Direction.DESC) {
                result[result.length - temp[data[i] - min]] = data[i];
            }
            else {
                result[temp[data[i] - min] - 1] = data[i];
            }
            --temp[data[i] - min];
        }
        return result;
    }
}
