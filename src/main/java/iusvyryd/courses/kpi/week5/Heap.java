package iusvyryd.courses.kpi.week5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Heap implementation
 */
public abstract class Heap<T extends Comparable<T>> {

    // name of heap
    String name;
    // list of elements
    List<T> keys = new ArrayList<>();
    // size of heap
    int size = 0;

    public Heap() {
    }

    public Heap(String name) {
        this(name, null);
    }

    public Heap(T[] arr) {
        this("", arr);
    }

    public Heap(String name, T[] arr) {
        this.name = name;
        build(arr);
    }

    /**
     * Build heap from array of comparable values
     * @param array
     */
    public void build(T[] array) {
        if (array == null) {
            return;
        }
        if (size > 0) {
            throw new IllegalStateException("already built");
        }
        this.size = array.length;
        this.keys.addAll(Arrays.asList(array));
        for (int i = (size - 1) >>> 1; i >= 0; i--) {
            heapify(i);
        }
    }

    /**
     * Return index of parent element
     * @param index current index
     * @return index of parent
     */
    public int parent(int index) {
        return (index - 1) / 2;
    }

    /**
     * Return index of left element
     * @param index current index
     * @return index of left
     */
    public int left(int index) {
        return 2 * index + 1;
    }

    /**
     * Return index of right element
     * @param index current index
     * @return index of right
     */
    public int right(int index) {
        return 2 * index + 2;
    }

    /**
     * Return key value in root element
     * @return key
     */
    public T root() {
        return keys.size() > 0 ? keys.get(0) : null;
    }

    /**
     * Return value of key in root and remove key in root
     * @return key
     */
    public T extractRoot() {
        if (this.size < 1) {
            throw new IllegalStateException("empty");
        }
        T root = root();
        keys.set(0, keys.get(size - 1));
        keys.remove(size - 1);
        size--;
        heapify(0);
        return root;
    }

    /**
     * Check and normalize element in index
     * @param index
     */
    public abstract void heapify(int index);

    /**
     * Insert new key and normalize heap
     * @param key
     */
    public abstract void insert(T key);

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(name);
        sb.append(": [");
        for (int i = 0; i < size; i++) {
            sb.append(keys.get(i));
            if (i != size - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
