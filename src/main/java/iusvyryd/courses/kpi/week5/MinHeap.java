package iusvyryd.courses.kpi.week5;

import iusvyryd.courses.kpi.AlgorithmUtils;

/**
 * Min Heap implementation
 * The keys of parent nodes are always smaller than or equal to those of
 * the children and the smallest key is in the root node
 */
public class MinHeap<T extends Comparable<T>> extends Heap<T> {

    public MinHeap() {
        super();
    }

    public MinHeap(T[] array) {
        super(array);
    }

    public MinHeap(String name) {
        super(name);
    }

    public MinHeap(String name, T[] array) {
        super(name, array);
    }

    @Override
    public void heapify(int index) {
        int l = left(index);
        int r = right(index);
        int smallest;
        if (l <= size - 1 && keys.get(l).compareTo(keys.get(index)) < 0) {
            smallest = l;
        }
        else {
            smallest = index;
        }
        if (r <= size - 1 && keys.get(r).compareTo(keys.get(smallest)) < 0) {
            smallest = r;
        }
        if (smallest != index) {
            AlgorithmUtils.swap(keys, smallest, index);
            heapify(smallest);
        }
    }

    @Override
    public void insert(T key) {
        size++;
        keys.add(key);
        decrease(size - 1, key);
    }

    public T max() {
        return root();
    }

    public T extractMin() {
        return extractRoot();
    }

    public void decrease(int index, T key) {
        if (keys.get(index).compareTo(key) < 0) {
            throw new IllegalArgumentException("larger then existed");
        }
        keys.set(index, key);
        while (index > 0 && keys.get(parent(index)).compareTo(keys.get(index)) > 0) {
            AlgorithmUtils.swap(keys, index, parent(index));
            index = parent(index);
        }
    }

}
