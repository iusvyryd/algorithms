package iusvyryd.courses.kpi.week5;

import iusvyryd.courses.kpi.AbstractSortingAlgorithm;
import iusvyryd.courses.kpi.AlgorithmUtils;

/**
 * Heap sort algorithm implementation.
 * based on comparison
 *
 * Time complexity:
 * worst - n * log(n)
 * average - n * log(n)
 * best - n * log(n)
 *
 * additional memory - 1
 * stable - false
 *
 */
public class HeapSort<T extends Comparable<T>> extends AbstractSortingAlgorithm<T> {

    public HeapSort() {
        super();
    }

    public HeapSort(Direction direction) {
        super(direction);
    }

    @Override
    public T[] sort(T[] data) {
        if (data == null) {
            return null;
        }

        Heap<T> heap = direction == Direction.DESC ? new MinHeap<>(data) : new MaxHeap<>(data);
        for (int i = data.length - 1; i > 0; i--){
            AlgorithmUtils.swap(heap.keys, 0, i);
            heap.size--;
            heap.heapify(0);
        }
        return heap.keys.toArray(data);
    }

}
