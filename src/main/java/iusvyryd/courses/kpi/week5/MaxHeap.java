package iusvyryd.courses.kpi.week5;

import iusvyryd.courses.kpi.AlgorithmUtils;

/**
 * Max Heap implementation
 * The keys of parent nodes are always greater than or equal to those of
 * the children and the highest key is in the root node
 */
public class MaxHeap<T extends Comparable<T>> extends Heap<T> {

    public MaxHeap() {
        super();
    }

    public MaxHeap(T[] array) {
        super(array);
    }

    public MaxHeap(String name) {
        super(name);
    }

    public MaxHeap(String name, T[] array) {
        super(name, array);
    }

    @Override
    public void heapify(int index) {
        int l = left(index);
        int r = right(index);
        int largest;
        if (l <= size - 1 && keys.get(l).compareTo(keys.get(index)) > 0) {
            largest = l;
        }
        else {
            largest = index;
        }
        if (r <= size - 1 && keys.get(r).compareTo(keys.get(largest)) > 0) {
            largest = r;
        }
        if (largest != index) {
            AlgorithmUtils.swap(keys, largest, index);
            heapify(largest);
        }
    }

    @Override
    public void insert(T key) {
        size++;
        keys.add(key);
        increase(size - 1, key);
    }

    public T max() {
        return root();
    }

    public T extractMax() {
        return extractRoot();
    }

    public void increase(int index, T key) {
        if (keys.get(index).compareTo(key) > 0) {
            throw new IllegalArgumentException("less then existed");
        }
        keys.set(index, key);
        while (index > 0 && keys.get(parent(index)).compareTo(keys.get(index)) < 0) {
            AlgorithmUtils.swap(keys, index, parent(index));
            index = parent(index);
        }
    }

}
