package iusvyryd.courses.kpi;

/**
 * This class provides base skeleton of sorting algorithm
 * To implement algorithm you need to extend this class
 *
 * @param <T> the type of object in array to be sort
 */
public abstract class AbstractSortingAlgorithm<T extends Comparable<T>>
        implements SortingAlgorithm<T> {

    /**
     * Direction to array will be sort
     */
    protected final Direction direction;

    /**
     * Comparisons counter
     */
    private int comparisonsCount = 0;

    public AbstractSortingAlgorithm() {
        this(Direction.ASC);
    }

    public AbstractSortingAlgorithm(Direction direction) {
        this.direction = direction;
    }

    @Override
    public int comparisonsCount() {
        return comparisonsCount;
    }

    /**
     * Checks if two elements is already sorted in specified direction
     *
     * @param first
     * @param second
     * @return true if first and second is sorted
     *         false otherwise
     */
    protected boolean isSorted(T first, T second) {
        comparisonsCount++;

        if (first == null) {
            return false;
        }
        else if (second == null) {
            return true;
        }
        if (direction == Direction.ASC) {
            return first.compareTo(second) <= 0;
        }
        else {
            return first.compareTo(second) >= 0;
        }
    }

}
