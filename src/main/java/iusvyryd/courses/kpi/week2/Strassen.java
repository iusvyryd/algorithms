package iusvyryd.courses.kpi.week2;

import iusvyryd.courses.kpi.AlgorithmUtils;

/**
 * Class implements Strassen quick multiplication algorithm
 * for big matrix
 */
public class Strassen {

    private static final int DEFAULT_THRESHOLD = 64;

    /**
     * threshold for triggering to common multiplication from MatrixUtil
     */
    private final int threshold;

    public Strassen() {
        this(DEFAULT_THRESHOLD);
    }

    public Strassen(int threshold) {
        this.threshold = threshold;
    }

    /**
     * Multiplication preparations
     *
     * @param a matrix A
     * @param b matrix B
     * @return multiply result
     *
     * @throws IllegalArgumentException if two matrix cannot be multiplied
     */
    public int[][] multiply(int[][] a, int[][] b) {
        int arows = a.length;
        int acols = a[0].length;
        int brows = b.length;
        int bcols = b[0].length;
        if (acols != brows) {
            throw new IllegalArgumentException("acols != brows");
        }

        if (acols <= threshold) {
            return MatrixUtil.multiply(a, b);
        }

        int n = AlgorithmUtils.nextPowerOf2(Math.max(Math.max(arows, acols), bcols));
        int[][] _a = squarify(a, n);
        int[][] _b = squarify(b, n);
        int[][] _c = strassenMultRec(_a, _b);

        int[][] c = new int[arows][bcols];
        for (int i = 0; i < arows; i++) {
            for (int j = 0; j < bcols; j++) {
                c[i][j] = _c[i][j];
            }
        }
        return c;
    }

    /**
     * Strassen recursive multiplication algorithm
     *
     * @param a matrix
     * @param b matirx
     * @return a * b
     */
    private int[][] strassenMultRec(int[][] a, int[][] b) {
        int n = a.length;
        if (n <= threshold) {
            return MatrixUtil.multiply(a, b);
        }

        int newN = n / 2;
        int[][] a11 = new int[newN][newN];
        int[][] a12 = new int[newN][newN];
        int[][] a21 = new int[newN][newN];
        int[][] a22 = new int[newN][newN];
        int[][] b11 = new int[newN][newN];
        int[][] b12 = new int[newN][newN];
        int[][] b21 = new int[newN][newN];
        int[][] b22 = new int[newN][newN];

        for (int i = 0; i < newN; i++) {
            for (int j = 0; j < newN; j++) {
                a11[i][j] = a[i][j];
                a12[i][j] = a[i][j + newN];
                a21[i][j] = a[i + newN][j];
                a22[i][j] = a[i + newN][j + newN];

                b11[i][j] = b[i][j];
                b12[i][j] = b[i][j + newN];
                b21[i][j] = b[i + newN][j];
                b22[i][j] = b[i + newN][j + newN];
            }
        }

        // (a11 + a22) * (b11 + b22)
        int[][] m1 = strassenMultRec(MatrixUtil.add(a11, a22), MatrixUtil.add(b11, b22));
        // (a21 + a22) * b11
        int[][] m2 = strassenMultRec(MatrixUtil.add(a21, a22), b11);
        // a11 * (b12 - b22)
        int[][] m3 = strassenMultRec(a11, MatrixUtil.subtract(b12, b22));
        // a22 * (b21 - b11)
        int[][] m4 = strassenMultRec(a22, MatrixUtil.subtract(b21, b11));
        // (a11 + a12) * b22
        int[][] m5 = strassenMultRec(MatrixUtil.add(a11, a12), b22);
        // (a21 - a11) * (b11 + b12)
        int[][] m6 = strassenMultRec(MatrixUtil.subtract(a21, a11), MatrixUtil.add(b11, b12));
        // (a12 - a22) * (b21 + b22)
        int[][] m7 = strassenMultRec(MatrixUtil.subtract(a12, a22), MatrixUtil.add(b21, b22));

        // c11 = m1 + m4 - m5 + m7
        int[][] c11 = MatrixUtil.add(m1, MatrixUtil.add(MatrixUtil.subtract(m4, m5), m7));
        // c12 = m3 + m5
        int[][] c12 = MatrixUtil.add(m3, m5);
        // c21 = m2 + m4
        int[][] c21 = MatrixUtil.add(m2, m4);
        // c22 = m1 - m2 + m3 + m6
        int[][] c22 = MatrixUtil.add(MatrixUtil.subtract(m1, m2), MatrixUtil.add(m3, m6));

        int[][] c = new int[n][n];
        for (int i = 0; i < newN; i++) {
            for (int j = 0; j < newN; j++) {
                c[i][j] = c11[i][j];
                c[i][j + newN] = c12[i][j];
                c[i + newN][j] = c21[i][j];
                c[i + newN][j + newN] = c22[i][j];
            }
        }
        return c;
    }

    /**
     * Makes from matrix a square matrix with n rows and cols
     *
     * @param a matrix
     * @param n number of rows and cols
     * @return squarified matrix
     */
    private int[][] squarify(int[][] a, int n) {
        int[][] arr = new int[n][n];
        for (int i = 0; i < a.length; i++) {
            System.arraycopy(a[i], 0, arr[i], 0, a[0].length);
        }
        return arr;
    }

}
