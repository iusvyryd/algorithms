package iusvyryd.courses.kpi.week2;

/**
 * Utility class with realisation of base matrix operations
 */
public class MatrixUtil {

    /**
     * Sum of two matrix
     *
     * @param a matrix A
     * @param b matrix B
     * @return a + b
     *
     * @throws IllegalArgumentException if number of rows or cols in matrix not same
     */
    public static int[][] add(int[][] a, int[][] b) {
        int arows = a.length;
        int acols = a[0].length;
        int brows = b.length;
        int bcols = b[0].length;
        if (arows != brows || acols != bcols) {
            throw new IllegalArgumentException("acols != bcols || arows != brows");
        }

        int[][] c = new int[arows][acols];
        for (int row = 0; row < arows; row++) {
            for (int col = 0; col < acols; col++) {
                c[row][col] = a[row][col] + b[row][col];
            }
        }

        return c;
    }

    /**
     * Subtraction of two
     *
     * @param a matrix A
     * @param b matrix B
     * @return a- b
     *
     * @throws IllegalArgumentException if number of rows or cols in matrix not same
     */
    public static int[][] subtract(int[][] a, int[][] b) {
        int arows = a.length;
        int acols = a[0].length;
        int brows = b.length;
        int bcols = b[0].length;
        if (arows != brows || acols != bcols) {
            throw new IllegalArgumentException("acols != bcols || arows != bcols");
        }

        int[][] c = new int[arows][acols];
        for (int row = 0; row < arows; row++) {
            for (int col = 0; col < acols; col++) {
                c[row][col] = a[row][col] - b[row][col];
            }
        }

        return c;
    }

    /**
     * Multiplication of two matrix
     *
     * @param a matrix A
     * @param b matrix B
     * @return a * b
     *
     * @throws IllegalArgumentException if number of columns in matrix a != number
     *                                  of rows in matrix B
     */
    public static int[][] multiply(int[][] a, int[][] b) {
        int arows = a.length;
        int acols = a[0].length;
        int brows = b.length;
        int bcols = b[0].length;
        if (acols != brows) {
            throw new IllegalArgumentException("acols != brows");
        }

        int [][] c = new int[arows][bcols];
        for (int row = 0; row < arows; row++) {
            for (int col = 0; col < bcols; col++) {
                for (int iter = 0; iter < brows; iter++) {
                    c[row][col] += a[row][iter] * b[iter][col];
                }
            }
        }

        return c;
    }

    /**
     * Print matrix in std out
     *
     * @param a matrix
     */
    public static void print(int[][] a) {
        for (int[] row : a) {
            for (int col : row) {
                System.out.print(col + "   ");
            }
            System.out.println();
        }
    }

}
