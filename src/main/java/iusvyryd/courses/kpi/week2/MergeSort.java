package iusvyryd.courses.kpi.week2;

import iusvyryd.courses.kpi.AbstractSortingAlgorithm;

/**
 * Merge sort algorithm implementation.
 * based on comparison
 *
 * Time complexity:
 * worst - n * log(n)
 * average - n * log(n)
 * best - n * log(n)
 *
 * additional memory - n (worst case)
 * stable - true
 *
 */
public class MergeSort<T extends Comparable<T>> extends AbstractSortingAlgorithm<T> {

    public MergeSort() {
        super();
    }

    public MergeSort(Direction direction) {
        super(direction);
    }

    @Override
    public T[] sort(T[] data) {
        if (data == null) {
            return null;
        }

        sort(data, 0, data.length - 1);
        return data;
    }

    private void sort(T[] data, int from, int to) {
        if (to - from <= 0) {
            return;
        }
        int c = (from + to) >>> 1;

        sort(data, from, c);
        sort(data, c + 1, to);
        merge(data, from, c, to);
    }

    private void merge(T[] data, int from, int c, int to) {
        int leftLen = c - from + 1;
        Object[] left = new Object[leftLen];
        for (int i = from, j = 0; j < leftLen; i++, j++) {
            left[j] = data[i];
        }

        int rightLen = to - c;
        Object[] right = new Object[rightLen];
        for (int i = c + 1, j = 0; j < rightLen; i++, j++) {
            right[j] = data[i];
        }

        int i = 0, j = 0, k = from;
        while (i < leftLen && j < rightLen) {
            if (isSorted((T)left[i], (T)right[j])) {
                data[k++] = (T)left[i++];
            } else {
                data[k++] = (T)right[j++];
            }
        }

        while (i < leftLen) {
            data[k++] = (T)left[i++];
        }
        while (j < rightLen) {
            data[k++] = (T)right[j++];
        }
    }

}
