package iusvyryd.courses.kpi.week2;

/**
 * Utility class that implements two inversions count algorithms
 */
public class InversionsCounter {

    /**
     * Max possible inversions count for array with length
     *
     * @param length length of array
     * @return max possible inversions counts
     */
    public static int maxForLength(int length) {
        return length * (length - 1) >>> 1;
    }

    /**
     * Brute method for inversion counting
     *
     * @param data array
     * @return count of inversion in data
     */
    public static <T extends Comparable<T>> int directCount(T[] data) {
        int cnt = 0;
        if (data != null) {
            for (int i = 0; i < data.length - 1; i++) {
                for (int j = i; j < data.length; j++) {
                    if (data[i].compareTo(data[j]) > 0) {
                        cnt++;
                    }
                }
            }
        }
        return cnt;
    }

    /**
     * Merge sort like inversion control algorithm
     *
     * @param data array
     * @return count of inversion in data
     */
    public static <T extends Comparable<T>> int decompositionCount(T[] data) {
        if (data == null) {
            return 0;
        }
        return decompositionCount(data, 0, data.length - 1);
    }

    private static <T extends Comparable<T>> int decompositionCount(T[] data, int from, int to) {
        if (to - from <= 0) {
            return 0;
        }
        int cnt = 0;
        if (data != null && to - from > 0) {
            int c = (from + to) >>> 1;
            cnt += decompositionCount(data, from, c);           // i < j <= n/2
            cnt += decompositionCount(data, c + 1, to);         // n/2 < i < j
            cnt += mergeAndCount(data, from, c, to);            // i <= n/2 < j
        }
        return cnt;
    }

    private static <T extends Comparable<T>> int mergeAndCount(T[] data, int from, int c, int to) {
        int cnt = 0;
        int leftLen = c - from + 1;
        Object[] left = new Object[leftLen];
        for (int i = from, j = 0; j < leftLen; i++, j++) {
            left[j] = data[i];
        }

        int rightLen = to - c;
        Object[] right = new Object[rightLen];
        for (int i = c + 1, j = 0; j < rightLen; i++, j++) {
            right[j] = data[i];
        }

        int i = 0, j = 0, k = from;
        while (i < leftLen && j < rightLen) {
            if (((T)left[i]).compareTo((T)right[j]) <= 0) {
                data[k++] = (T)left[i++];
            } else {
                data[k++] = (T)right[j++];
                cnt += leftLen - i;
            }
        }

        while (i < leftLen) {
            data[k++] = (T)left[i++];
        }
        while (j < rightLen) {
            data[k++] = (T)right[j++];
        }
        return cnt;
    }


}
