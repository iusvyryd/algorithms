package iusvyryd.courses.kpi.week6;

import iusvyryd.courses.kpi.DataLoader;

import java.io.IOException;
import java.util.ArrayList;

/**
 * В даній задачі вам потрібно реалізувати алгоритм пошуку сум в хеш-таблиці,
 * який наводився в лекції цього тижня (відео-блок №2, "Приклади"). Нагадаємо,
 * що оригінально вона формулюється наступним чином: для заданого масиву цілих чисел A
 * та цілого числа S, необхідно перевірити, чи є в A два числа x та y, такі що x + y = S.
 *
 * У цьому завданні ця задача дещо ускладнена, але тільки в бік складності обрахунку
 * результату. Це зроблено зокрема для того, щоб ви могли дійсно відчути переваги
 * застосування хеш-таблиць.
 *
 * Отже, заданий вхідний файл, який містить 1 мільйон цілих чисел (кожне число
 * записане в окремий рядок). Числа можуть бути як додатні, так і від’ємні; також
 * можуть бути повторення чисел.
 *
 * Ваша задача полягає в обрахунку кількості можливих різних значень S в інтервалі
 * від [-1000,1000] (включно), таких що в заданому файлі є два різних за значенням
 * числа x та y і задовольняється умова x + y = S.
 */
public class PracticalTask6 {

    HashTable<Long> data;
    HashTable<Integer> ret = new HashTable<>(100);

    public PracticalTask6(int capacity, String res) throws IOException {
        data = new HashTable<>(capacity);
        Long[] arr = DataLoader.loadLongArray(res);
        for (Long ll : arr) {
            data.add(ll);
        }
    }

    public int getResult() {
        for (ArrayList al : data.buckets) {
            for (Object x : al) {
                for (int i = -1000; i <= 1000; i++) {
                    Long y = i - (Long)x;
                    if (data.contains(y)) {
                        ret.add(Integer.valueOf(i));
                    }
                }
            }
        }
        return ret.size();
    }

    public static void main(String[] args) throws IOException {
        PracticalTask6 pt6 = new PracticalTask6(1000000, "week6/input_06.txt");
        System.out.println("Ex1: " + pt6.getResult());
    }

}
