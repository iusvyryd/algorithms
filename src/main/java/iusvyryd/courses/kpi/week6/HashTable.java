package iusvyryd.courses.kpi.week6;

import java.util.ArrayList;

/**
 * Simple ineffective hash table implementation
 */
public class HashTable<T> {

    public static final int DEFAULT_CAPACITY = 100;

    public ArrayList[] buckets;
    public int capacity;

    public HashTable() {
        this(DEFAULT_CAPACITY);
    }

    public HashTable(int capacity) {
        this.capacity = capacity;
        buckets = new ArrayList[capacity];
        for (int i = 0; i < buckets.length; i++) {
            buckets[i] = new ArrayList();
        }
    }

    public void add(T l) {
        int hash = Math.abs(l.hashCode()) % capacity;
        ArrayList al = buckets[hash];
        if (!al.contains(l)) {
            al.add(l);
        }
    }

    public int size() {
        int sz = 0;
        for (ArrayList al : buckets) {
            sz += al.size();
        }
        return sz;
    }

    public boolean contains(T l) {
        int hash = Math.abs(l.hashCode()) % capacity;
        ArrayList al = buckets[hash];
        return al.contains(l);
    }
}
