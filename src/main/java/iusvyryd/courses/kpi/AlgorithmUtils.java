package iusvyryd.courses.kpi;

import java.util.List;

/**
 * This class provides the number of useful static methods
 * that helps to implement algorithms
 */
public class AlgorithmUtils {

    private AlgorithmUtils() {
    }

    /**
     * Utility class that represents pair of values
     */
    public static class Pair<T, E> {
        private T first;
        private E second;
        public Pair(T first, E second) {
            this.first = first;
            this.second = second;
        }
        public T first() {
            return first;
        }
        public E second() {
            return second;
        }
    }

    /**
     * Utility class that represents triple of values
     */
    public static class Triple<T, E, G> extends Pair<T, E> {
        private G third;
        public Triple(T first, E second, G third) {
            super(first, second);
            this.third = third;
        }
        public G third() {
            return third;
        }
    }

    /**
     * Helps to swap two elements in array
     *
     * @param data array of type T
     * @param first element that will swap with second
     * @param second element that will swap with first
     * @param <T> type of array objects
     */
    public static <T> void swap(T[] data, int first, int second) {
        T buf = data[first];
        data[first] = data[second];
        data[second] = buf;
    }

    /**
     * Helps to swap two elements in List
     *
     * @param data list
     * @param first element that will swap with second
     * @param second element that will swap with first
     * @param <T> type of list elements
     */
    public static <T> void swap(List<T> data, int first, int second) {
        T buf = data.get(first);
        data.set(first, data.get(second));
        data.set(second, buf);
    }

    /**
     * Return next power of 2 nearest to num
     *
     * @param num
     * @return next power of 2
     */
    public static int nextPowerOf2(int num) {
        return (int)Math.pow(2, Math.ceil(Math.log(num) / Math.log(2)));
    }

}
