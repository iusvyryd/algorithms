package iusvyryd.courses.kpi.week7;

import iusvyryd.courses.kpi.DataLoader;
import iusvyryd.courses.kpi.week1.InsertionSort;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Дана задачі розбивається на два завдання.
 *
 * 1. ПЕРЕТВОРИТИ ВХІДНЕ БІНАРНЕ ДЕРЕВО У БІНАРНЕ ДЕРЕВО ПОШУКУ
 *
 * На вхід подається деяке бінарне дерево, із фіксованою структурою (тобто зв’язками між вузлами,
 * їх батьком та нащадками). Необхідно переписати значення вузлів дерева таким чином, щоби:
 *
 * a) їх нові значення брались тільки з того набору, який присутній у вхідному дереві;
 * b) зберігалась внутрішня структура дерева (зберігались зв’язки між кожним батьківським вузлом та
 * його вузлами нащадками).
 *
 * Розв’язати цю задачу можна за допомогою наступного алгоритму:
 *
 * Обійти задане дерево у внутрішньому порядку (in-order) та зберігати всі значення в масиві.
 * Відсортувати масив у зростаючому порядку.
 * Знову обійти дерево у внутрішньому порядку та послідовно вписати значення з відсортованого
 * масиву у вузли дерева за порядком обходу.
 *
 * 2. ПОШУК СУМ ПОСЛІДОВНИХ ВУЗЛІВ В ДЕРЕВІ
 *
 * Після того, як вхідне дерево перетворене на бінарне дерево пошуку, необхідно розв’язати наступну задачу.
 * Додатково задається деяке число S. В отриманому бінарному дереві пошуку необхідно знайти всі такі
 * монотонні шляхи (які не обов'язково йдуть від кореня, але всі прямують згори донизу), що сума значень
 * вузлів, які належать знайденим шляхам, дорівнює числу S.
 *
 * 1)
 * Ви повинні розв’язати поставлену задачу перетворення заданого бінарного дерева у бінарне дерево
 * пошуку для заданого вхідного файлу. Опис формату вхідного файлу дивіться на попередній сторінці,
 * де наводиться постановка задачі.
 * Тут ми перевіримо, чи правильно ви побудували бінарне дерево пошуку, на основі кількох контрольних
 * точок. Спершу, введіть значення ключа, яке зберігається у корені.
 * Тепер введіть через пробіл перші три зліва листка дерева.
 * Тепер введіть через пробіл останні три листка дерева (якщо знову ж таки дивитись зліва).
 *
 * 2)
 * Перейдемо до другого завдання. Вам потрібно знайти всі монотонні шляхи, які спрямовані в дереві
 * згори донизу, і сума вузлів в яких дорівнює певному вхідному числу. Монотонність означає,
 * що шлях не повинен перериватись, а йти від батька до нащадка і т.д. Вхідний файл той самий.
 * Для всіх завдань нижче передбачається, що монотонні шляхи є в єдиному екземплярі.
 * Введіть через пробіл значення ключів для вузлів у монотонному шляху для суми 1059
 * Введіть через пробіл значення ключів для вузлів у монотонному шляху для суми 1546
 * Введіть через пробіл значення ключів для вузлів у монотонному шляху для суми 1940
 *
 */
public class PracticalTask7 {

    private class Node {
        int key;
        Node parent;
        Node left;
        Node right;
        Node(int k, Node p, Node l, Node r) {
            this.key = k;
            this.parent = p;
            this.left = l;
            this.right = r;
        }
    }

    int size;
    Node root;

    /**
     * Populate tree from array
     * @param arr
     */
    public void treeFromArray(int[] arr) {
        Node prev = null;
        boolean right = false;
        for (int i = 0; i < arr.length; i++) {
            if (i == 0) {
                root = new Node(arr[i], prev, null, null);
                size++;
                prev = root;
                continue;
            }

            if (arr[i] == 0) {
                if (!right) {
                    right = true;
                    continue;
                }
                else {
                    prev = prev.parent;
                    while (prev != null && prev.right != null) {
                        prev = prev.parent;
                    }
                    continue;
                }
            }

            Node n = new Node(arr[i], prev, null, null);
            size++;
            if (right) {
                prev.right = n;
                right = false;
            }
            else {
                prev.left = n;
            }
            prev = n;
        }
    }

    /**
     * Check if current nodes sequence is binary search tree
     * @return
     */
    public boolean isBST() {
        return isBST(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }
    private boolean isBST(Node n, int min, int max) {
        if (n == null) return true;
        if (n.key < min || n.key > max) return false;
        return isBST(n.left, min, n.key) && isBST(n.right, n.key, max);
    }

    /**
     * Create array from tree using in-order walk
     * @return
     */
    public int[] toInorderArray() {
        int[] arr = new int[size];
        toInorderArray(root, arr, 0);
        return arr;
    }
    private int toInorderArray(Node from, int[] arr, int i) {
        if (from != null) {
            i = toInorderArray(from.left, arr, i);
            arr[i++] = from.key;
            i = toInorderArray(from.right, arr, i);
        }
        return i;
    }

    /**
     * Create array from tree using pre-order walk
     * @return
     */
    public void toPreorderArray() {
        toPreorderArray(root);
    }
    private void toPreorderArray(Node from) {
        if (from != null) {
            System.out.printf(from.key + " ");
            toPreorderArray(from.left);
            toPreorderArray(from.right);
        }
        else {
            System.out.printf("0 ");
        }
    }

    /**
     * Transform current tree structure to BSTusing additional sorted array
     * @param arr
     */
    public void transformToBST(int[] arr) {
        transformToBST(root, arr, 0);
    }
    private int transformToBST(Node from, int[] arr, int i) {
        if (from != null) {
            i = transformToBST(from.left, arr, i);
            from.key = arr[i++];
            i = transformToBST(from.right, arr, i);
        }
        return i;
    }

    /**
     * Return array of all tree leafs from left to right
     * @return
     */
    public Integer[] allLeafs() {
        List<Integer> list = new ArrayList<>();
        allLeafs(root, list);
        return list.toArray(new Integer[list.size()]);
    }
    private void allLeafs(Node from, List<Integer> list) {
        if (from != null) {
            allLeafs(from.left, list);
            if (from.left == null && from.right == null) {
                list.add(from.key);
            }
            allLeafs(from.right, list);
        }
    }

    /**
     * Find all keys sequence in mono path which keys gives a specified sum
     * @param sum
     * @return
     */
    public List<List<Integer>> monoPaths(int sum) {
        List<List<Integer>> list = new ArrayList<>();
        monoPaths(root, sum, list);
        return list;
    }
    private void monoPaths(Node from, int sum, List<List<Integer>> list) {
        if (from != null) {
            Node x = from;
            int s = 0;
            List<Integer> l = new ArrayList<>();
            while (x != null) {
                l.add(x.key);
                s += x.key;
                if (s == sum) {
                    Collections.reverse(l);
                    list.add(l);
                    break;
                }
                x = x.parent;
            }
            monoPaths(from.left, sum, list);
            monoPaths(from.right, sum, list);
        }
    }

    public static void main(String[] args) throws IOException {
        int[] array = DataLoader.loadIntArrayFromSpacedOneLine("week7/input_07.txt");

        PracticalTask7 pt = new PracticalTask7();
        pt.treeFromArray(array);

        int[] arrayFromTree = pt.toInorderArray();
        Arrays.sort(arrayFromTree);
        pt.transformToBST(arrayFromTree);

        System.out.println("Ex1:");
        System.out.println("Head: " + pt.root.key);
        Integer[] leafs = pt.allLeafs();
        System.out.println("Leaf first 3: " + leafs[0] + " " + leafs[1] + " " + leafs[2]);
        System.out.println("Leaf last 3: " + leafs[leafs.length - 3] + " " + leafs[leafs.length - 2] + " " + leafs[leafs.length - 1]);

        System.out.println("Ex2:");
        System.out.println("1059:");
        List<List<Integer>> ll = pt.monoPaths(1059);
        for (List<Integer> li : ll) {
            System.out.println(li);
        }
        System.out.println("1546:");
        ll = pt.monoPaths(1546);
        for (List<Integer> li : ll) {
            System.out.println(li);
        }
        System.out.println("1940:");
        ll = pt.monoPaths(1940);
        for (List<Integer> li : ll) {
            System.out.println(li);
        }
    }
}
