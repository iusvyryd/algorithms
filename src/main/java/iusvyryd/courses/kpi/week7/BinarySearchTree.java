package iusvyryd.courses.kpi.week7;

import java.util.ArrayList;
import java.util.List;

/**
 * Binary search tree implementation
 */
public class BinarySearchTree<T extends Comparable<T>> {

    private class Node {
        T key;
        Node parent;
        Node left;
        Node right;
        Node(T key, Node parent, Node left, Node right) {
            this.key = key;
            this.parent = parent;
            this.left = left;
            this.right = right;
        }
    }

    private Node root;

    public BinarySearchTree() {
    }

    /**
     * Check if tree contains key
     * @param key key to check
     * @return
     */
    public boolean contains(T key) {
        return search(root, key) != null;
    }

    private Node search(Node from, T key) {
        if (from == null || key.compareTo(from.key) == 0) {
            return from;
        }
        if (key.compareTo(from.key) < 0) {
            return search(from.left, key);
        }
        else {
            return search(from.right, key);
        }
    }

    private Node iterativeSearch(Node from, T key) {
        Node cur = from;
        while (cur != null && key.compareTo(cur.key) != 0) {
            if (key.compareTo(cur.key) < 0) {
                cur = cur.left;
            }
            else {
                cur = cur.right;
            }
        }
        return cur;
    }

    /**
     * Search for minimum key in tree
     * @return
     */
    public T min() {
        return min(root).key;
    }
    private Node min(Node from) {
        if (from.left == null) {
            return from;
        }
        return min(from.left);
    }

    /**
     * Search for maximum key in tree
     * @return
     */
    public T max() {
        return max(root).key;
    }
    private Node max(Node from) {
        if (from.right == null) {
            return from;
        }
        return max(from.right);
    }

    /**
     * array by in-order tree walk
     * @return array
     */
    public List<T> inorderWalk() {
        List<T> list = new ArrayList<>();
        inorderWalk(root, list);
        return list;
    }

    private void inorderWalk(Node from, List<T> list) {
        if (from != null) {
            inorderWalk(from.left, list);
            list.add(from.key);
            inorderWalk(from.right, list);
        }
    }

    /**
     * array by pre-order tree walk
     * @return array
     */
    public List<T> preorderWalk() {
        List<T> list = new ArrayList<>();
        preorderWalk(root, list);
        return list;
    }

    private void preorderWalk(Node from, List<T> list) {
        if (from != null) {
            list.add(from.key);
            preorderWalk(from.left, list);
            preorderWalk(from.right, list);
        }
    }

    /**
     * array by post-order tree walk
     * @return array
     */
    public List<T> postorderWalk() {
        List<T> list = new ArrayList<>();
        postorderWalk(root, list);
        return list;
    }

    private void postorderWalk(Node from, List<T> list) {
        if (from != null) {
            postorderWalk(from.left, list);
            postorderWalk(from.right, list);
            list.add(from.key);
        }
    }

    private Node nextNode(Node from) {
        if (from.right != null) {
            return min(from.right);
        }
        Node y = from.parent;
        while (y != null && y.right.key.compareTo(from.key) == 0) {
            from = y;
            y = y.parent;
        }
        return y;
    }

    private Node prevNode(Node from) {
        if (from.left != null) {
            return max(from.left);
        }
        Node y = from.parent;
        while (y != null && y.left.key.compareTo(from.key) == 0) {
            from = y;
            y = y.parent;
        }
        return y;
    }

    public void insert(T key) {
        Node y = null;
        Node x = root;
        while (x != null) {
            y = x;
            if (key.compareTo(x.key) <= 0) {
                x = x.left;
            }
            else {
                x = x.right;
            }
        }
        Node z = new Node(key, y, null, null);
        if (y == null) {
            root = z;
        }
        else if (key.compareTo(y.key) <= 0) {
            y.left = z;
        }
        else {
            y.right = z;
        }
    }

    public void delete(T key) {
        Node z = search(root, key);
        if (z == null) {
            return;
        }
        // 1. n is leaf
        if (z.left == null && z.right == null) {
            if (z.parent != null) {
                if (z.parent.left.key.compareTo(key) == 0) {
                    z.parent.left = null;
                } else {
                    z.parent.right = null;
                }
            } else {
                root = null;
            }
        }
        // 2. n has only left or only right
        else if ((z.left == null && z.right != null) || (z.left != null && z.right == null)) {
            Node x;
            if (z.left != null) {
                x = z.left;
            }
            else {
                x = z.right;
            }
            if (z.parent != null) {
                if (z.parent.left.key.compareTo(key) == 0) {
                    z.parent.left = x;
                }
                else {
                    z.parent.right = x;
                }
                x.parent = z.parent;
            }
            else {
                root = x;
            }
        }
        // 3. there is left and right
        else {
            Node y = nextNode(z);
            Node x = y.right;
            if (x != null) {
                x.parent = y.parent;
                x.parent.left = x;
                z.key = y.key;
            }
        }
    }

}
