package iusvyryd.courses.kpi.week3;

import java.util.Random;

public class QuickSortBaseSelectionStrategies {

    public enum StrategyType {
        LAST,
        FIRST,
        MIDDLE,
        FIRST_MIDDLE_LAST_MEDIAN,
        RANDOM,
    }

    public static class Factory {
        public static QuickSortBaseSelectionStrategy strategy(StrategyType type) {
            switch (type) {
            case LAST:
                return new LastStrategy();
            case FIRST:
                return new FirstStrategy();
            case MIDDLE:
                return new MiddleStrategy();
            case FIRST_MIDDLE_LAST_MEDIAN:
                return new MedianStrategy();
            case RANDOM:
                return new RandomStrategy();
            default:
                throw new IllegalArgumentException("unknown type");
            }
        }
    }

    // Last element as base
    private static class LastStrategy<T extends Comparable<T>>
            implements QuickSortBaseSelectionStrategy<T>
    {
        @Override
        public int selectBaseIndex(T[] data, int from, int to) {
            return to;
        }
    }

    // First element as base
    private static class FirstStrategy<T extends Comparable<T>>
            implements QuickSortBaseSelectionStrategy<T>
    {
        @Override
        public int selectBaseIndex(T[] data, int from, int to) {
            return from;
        }
    }

    // Middle element as base
    private static class MiddleStrategy<T extends Comparable<T>>
            implements QuickSortBaseSelectionStrategy<T>
    {
        @Override
        public int selectBaseIndex(T[] data, int from, int to) {
            return (from + to) >>> 1;
        }
    }

    // First, middle, last median as base
    private static class MedianStrategy<T extends Comparable<T>>
            implements QuickSortBaseSelectionStrategy<T>
    {
        @Override
        public int selectBaseIndex(T[] data, int from, int to) {
            int mid = (from + to) >>> 1;
            int median;
            if (data[from].compareTo(data[mid]) < 0 &&
                    data[from].compareTo(data[to]) < 0) {
                median = data[mid].compareTo(data[to]) < 0 ? mid : to;
            }
            else if (data[mid].compareTo(data[from]) < 0 &&
                    data[mid].compareTo(data[to]) < 0) {
                median = data[from].compareTo(data[to]) < 0 ? from : to;
            }
            else {
                median = data[mid].compareTo(data[from]) < 0 ? mid : from;
            }
            return median;
        }
    }

    // Random as base
    private static class RandomStrategy<T extends Comparable<T>>
            implements QuickSortBaseSelectionStrategy<T>
    {
        @Override
        public int selectBaseIndex(T[] data, int from, int to) {
            if (from >= to) {
                return from;
            }
            Random rand = new Random();
            return rand.nextInt((to - from) + 1) + from;
        }
    }
}
