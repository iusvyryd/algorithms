package iusvyryd.courses.kpi.week3;

import iusvyryd.courses.kpi.AbstractSortingAlgorithm;
import iusvyryd.courses.kpi.AlgorithmUtils;

/**
 * Quick sort algorithm implementation.
 * based on comparison
 *
 * Time complexity:
 * worst - n^2
 * average - n * log(n)
 * best - n * log(n)
 *
 * additional memory - lon(n) (average), n (worst)
 * stable - false
 *
 */
public class QuickSort<T extends Comparable<T>> extends AbstractSortingAlgorithm<T> {

    private QuickSortBaseSelectionStrategy strategy;

    public QuickSort() {
        this(Direction.ASC);
    }

    public QuickSort(Direction direction) {
        this(direction, QuickSortBaseSelectionStrategies.Factory
                .strategy(QuickSortBaseSelectionStrategies.StrategyType.MIDDLE));
    }

    public QuickSort(Direction direction, QuickSortBaseSelectionStrategy strategy) {
        super(direction);
        this.strategy = strategy;
    }

    @Override
    public T[] sort(T[] data) {
        if (data == null) {
            return null;
        }

        sort(data, 0, data.length - 1);
        return data;
    }

    private void sort(T[] data, int from, int to) {
        if (data == null || to < from) {
            return;
        }
        int q = partition(data, from, to);
        sort(data, from, q-1);
        sort(data, q+1, to);
    }

    private int partition(T[] data, int from, int to) {
        int baseIdx = this.strategy.selectBaseIndex(data, from, to);
        // swap with last
        AlgorithmUtils.swap(data, baseIdx, to);
        // always select last as base
        T base = data[to];
        int i = from - 1;
        for (int j = from; j < to; j++) {
            if (isSorted(data[j], base)) {
                i++;
                AlgorithmUtils.swap(data, i, j);
            }
        }
        AlgorithmUtils.swap(data, to, ++i);
        return i;
    }
}
