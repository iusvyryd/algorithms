package iusvyryd.courses.kpi.week3;

public interface QuickSortBaseSelectionStrategy<T extends Comparable<T>> {

    public int selectBaseIndex(T[] data, int from, int to);

}
