package iusvyryd.courses.kpi.week3;

import iusvyryd.courses.kpi.AlgorithmUtils;

/**
 * Utility class with methods to help search order statistics in array
 */
public class OrderStatisticsUtils {

    /**
     * Find out min and max values in array
     * @param data input array to find min and max in
     * @return <code>Pair</code> min and max
     */
    public static <T extends Comparable<T>> AlgorithmUtils.Pair<T, T> minAndMax(T[] data) {
        if (data == null) {
            return null;
        }
        if (data.length < 2) {
            return new AlgorithmUtils.Pair<T, T>(data[0], data[0]);
        }

        int pos;
        T min, max;
        if (data.length % 2 == 0) {
            if (data[0].compareTo(data[1]) < 0) {
                min = data[0]; max = data[1];
            }
            else {
                min = data[1]; max = data[0];
            }
            pos = 2;
        }
        else {
            min = max = data[0];
            pos = 1;
        }

        while (pos < data.length) {
            if (data[pos].compareTo(data[pos + 1]) <= 0) {
                if (min.compareTo(data[pos]) > 0) {
                    min = data[pos];
                }
                if (max.compareTo(data[pos + 1]) < 0) {
                    max = data[pos + 1];
                }
            }
            else {
                if (min.compareTo(data[pos + 1]) > 0) {
                    min = data[pos + 1];
                }
                if (max.compareTo(data[pos]) < 0) {
                    max = data[pos];
                }
            }
            pos += 2;
        }

        return new AlgorithmUtils.Pair<T, T>(min, max);
    }

    /**
     * Find order statistics in array (n-th element in sorted array)
     * @param data array
     * @param n index of order statistics element
     * @return element in n-th position
     */
    public static <T extends Comparable<T>> T findOrderStatistics(T[] data, int n) {
        return findOrderStatistics(data, 0, data.length - 1, n);
    }

    public static <T extends Comparable<T>> T findOrderStatistics(T[] data, int from, int to, int n) {
        if (data == null) {
            return null;
        }
        if (from == to) {
            return data[from];
        }

        int q = partition(data, from, to);
        int k = q - from;
        if (n < k) {
            return findOrderStatistics(data, from, q - 1, n);
        }
        else if (n > k) {
            return findOrderStatistics(data, q + 1, to, n - k);
        }
        else {
            return data[q];
        }
    }

    private static <T extends Comparable<T>> int partition(T[] data, int from, int to) {
        T base = data[to];
        int i = from - 1;
        for (int j = from; j < to; j++) {
            if (data[j].compareTo(base) <= 0) {
                i++;
                AlgorithmUtils.swap(data, i, j);
            }
        }
        AlgorithmUtils.swap(data, to, ++i);
        return i;
    }

}
