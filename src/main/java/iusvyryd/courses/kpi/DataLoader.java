package iusvyryd.courses.kpi;

import org.omg.CORBA.INTERNAL;

import java.io.*;
import java.util.*;

/**
 * Helper class that helps to load resource data files
 */
public final class DataLoader {

    private DataLoader() {}

    public static Integer[] loadIntegerArray(String res) throws IOException {
        File file = new File(ClassLoader.getSystemClassLoader()
                .getResource(res).getFile());
        BufferedReader reader = new BufferedReader(new FileReader(file));

        String line = reader.readLine();
        int size = Integer.valueOf(line);

        Integer[] arr = new Integer[size];
        int cnt = 0;
        while ((line = reader.readLine()) != null && cnt < size) {
            arr[cnt] = Integer.valueOf(line);
            cnt++;
        }
        return arr;
    }

    public static Long[] loadLongArray(String res) throws IOException {
        File file = new File(ClassLoader.getSystemClassLoader()
                .getResource(res).getFile());
        BufferedReader reader = new BufferedReader(new FileReader(file));

        String line = reader.readLine();
        int size = Integer.valueOf(line);

        Long[] arr = new Long[size];
        int cnt = 0;
        while ((line = reader.readLine()) != null && cnt < size) {
            arr[cnt] = Long.valueOf(line);
            cnt++;
        }
        return arr;
    }

    public static int[][] loadInt2DArray(String res) throws IOException {
        File file = new File(ClassLoader.getSystemClassLoader()
                .getResource(res).getFile());
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line = reader.readLine();

        String[] headerParts = line.split(" ");
        int dataSize = Integer.valueOf(headerParts[0]);
        int dataItemSize = Integer.valueOf(headerParts[1]);

        int[][] data = new int[dataSize][dataItemSize];
        int counter = 0;
        while ((line = reader.readLine()) != null && counter < dataSize) {
            String [] parts = line.split(" ");
            for (int i = 0; i < dataItemSize; i++) {
                data[counter][i] = Integer.valueOf(parts[i+1]);
            }
            counter++;
        }
        return data;
    }

    public static int[] loadIntArrayWithKnownZeroValue(String res, int zeroify, int size) throws IOException {
        File f = new File(ClassLoader.getSystemClassLoader().getResource(res).getFile());
        BufferedReader in = new BufferedReader(new FileReader(f));
        int[] arr = new int[size];
        int cnt = 0;
        String line;
        while ((line = in.readLine()) != null && cnt < size) {
            String[] parts = line.split(" ");
            if (cnt == zeroify) {
                cnt++;
            }
            arr[cnt] = Integer.valueOf(parts[1]);
            cnt++;
        }
        return arr;
    }

    public static String[] loadStringArrayWithUnknownSize(String res) throws IOException {
        File file = new File(ClassLoader.getSystemClassLoader().getResource(res).getFile());
        BufferedReader reader = new BufferedReader(new FileReader(file));
        List<String> list = new ArrayList<>();
        String line = null;
        while ((line = reader.readLine()) != null) {
            list.add(line);
        }
        return list.toArray(new String[list.size()]);
    }

    public static Map<Integer, AlgorithmUtils.Triple<String, String, String>> loadStringTripple(String res)
        throws IOException
    {
        File file = new File(ClassLoader.getSystemClassLoader().getResource(res).getFile());
        BufferedReader reader = new BufferedReader(new FileReader(file));
        Map<Integer, AlgorithmUtils.Triple<String, String, String>> m = new HashMap<>();
        int counter = 0;
        String line = null;
        while ((line = reader.readLine()) != null) {
            if (line.isEmpty()) {
                continue;
            }
            String first = line;
            String second = reader.readLine();
            String third = reader.readLine();
            m.put(++counter, new AlgorithmUtils.Triple(first, second, third));
        }
        return m;
    }

    public static int[] loadIntArrayFromSpacedOneLine(String res) throws IOException {
        File f = new File(ClassLoader.getSystemClassLoader().getResource(res).getFile());
        BufferedReader in = new BufferedReader(new FileReader(f));
        String line = in.readLine();
        String[] items = line.split(" ");
        int[] arr = new int[items.length];
        for (int i = 0; i < items.length; i++) {
            arr[i] = Integer.valueOf(items[i]).intValue();
        }
        return arr;
    }

    public static List<AlgorithmUtils.Pair<Integer, Integer>> loadIntegerPairs(String res) throws IOException {
        File f = new File(ClassLoader.getSystemClassLoader().getResource(res).getFile());

        List<AlgorithmUtils.Pair<Integer, Integer>> list = new LinkedList<>();
        BufferedReader reader = new BufferedReader(new FileReader(f));
        String line = null;
        while ((line = reader.readLine()) != null) {
            if (line.isEmpty()) {
                continue;
            }
            String[] items = line.split(" ");
            list.add(new AlgorithmUtils.Pair<>(Integer.valueOf(items[0]), Integer.valueOf(items[1])));
        }
        return list;
    }
}
