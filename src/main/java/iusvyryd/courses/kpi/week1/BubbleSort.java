package iusvyryd.courses.kpi.week1;

import iusvyryd.courses.kpi.AbstractSortingAlgorithm;
import iusvyryd.courses.kpi.AlgorithmUtils;

/**
 * Bubble sort algorithm implementation
 * based on comparison
 *
 * Time complexity:
 * worst - n^2
 * average - n^2
 * best - n
 *
 * additional memory - 1
 * stable - true
 *
 */
public class BubbleSort<T extends Comparable<T>> extends AbstractSortingAlgorithm<T> {

    public BubbleSort() {
        super();
    }

    public BubbleSort(Direction direction) {
        super(direction);
    }

    @Override
    public T[] sort(T[] data) {
        if (data == null) {
            return null;
        }

        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data.length - 1 - i; j++) {
                if (!isSorted(data[j], data[j+1])) {
                    AlgorithmUtils.swap(data, j, j+1);
                }
            }
        }
        return data;
    }
}
