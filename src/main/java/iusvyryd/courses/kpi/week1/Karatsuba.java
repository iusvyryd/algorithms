package iusvyryd.courses.kpi.week1;

import iusvyryd.courses.kpi.AlgorithmUtils;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

/**
 * Class implements Karatsuba quick multiplication algorithm
 * for big numbers
 */
public class Karatsuba {

    private static final int DEFAULT_THRESHOLD = 64;
    private static final BigInteger ten = BigInteger.valueOf(10);

    /**
     * threshold for triggering to common multiplication *
     */
    private final int threshold;

    /**
     * flag that enables statistics collection
     * for PracticalTask #1
     */
    private final boolean collectADplusBCStatistics;

    /**
     * storage for statistics
     * key - ad + bc
     * value - number of key occurrence
     */
    private Map<BigInteger, Integer> ADplusBCStatistics;

    public Karatsuba() {
        this(false);
    }

    public Karatsuba(boolean collectStatistics) {
        this(collectStatistics, DEFAULT_THRESHOLD);
    }

    public Karatsuba(boolean collectStatistics, int threshold) {
        this.collectADplusBCStatistics = collectStatistics;
        this.threshold = threshold;
    }

    /**
     * @param first multiplier
     * @param second multiplier
     * @return multiply result
     *
     * Example:
     * 1234 * 5678 = 7006652
     * a=12 b=34 c=56 d=78
     * (a*10^(n/2)+b) * (c*10^(n/2)+d)
     * 10^n*ac + 10^(n/2)*(ad + bc) + bd
     *
     * (a + b) * (c + d) = ac + ad + bc + bd
     * (ad + bc) = (a + b) * (c + d) - ac - bd
     *
     * 10^n * ac + 10^(n/2) * ((a + b) * (c + d) - ac - bd) + bd
     * 10000 * 672 + 100 * 2840 + 2652 = 7006652
     */
    public BigInteger multiply(BigInteger first, BigInteger second) {
        // check if use common multiplication
        if (useCommonMultiplication(first, second)) {
            return first.multiply(second);
        }

        // resolve n
        int n = resolveBaseLen(first, second);
        // 10^n
        BigInteger nllion = ten.pow(n);
        // 10^(n/2)
        BigInteger seminllion = ten.pow(n / 2);

        // separation first to a and b
        BigInteger a = first.divide(seminllion);
        BigInteger b = first.subtract(a.multiply(seminllion));
        // separation second to c and d
        BigInteger c = second.divide(seminllion);
        BigInteger d = second.subtract(c.multiply(seminllion));

        // part ac
        BigInteger ac = multiply(a, c);
        // part bd
        BigInteger bd = multiply(b, d);
        // part (ad + bc) = (a + b) * (c + d) - ac - bd
        BigInteger abPlusbc = multiply(a.add(b), c.add(d)).subtract(ac).subtract(bd);

        // collect ad+bc in statistics
        collectADplusBCStatistics(abPlusbc);

        // 10^n * ac + 10^(n/2) * (ad + bc) + bd
        return ac.multiply(nllion).add(abPlusbc.multiply(seminllion)).add(bd);
    }

    /**
     * Checks number of digits in multipliers with specified {@link #threshold threshold}
     *
     * @param first multiplier
     * @param second multiplier
     * @return if need to use common multiply method instead of karatsuba method
     */
    private boolean useCommonMultiplication(BigInteger first, BigInteger second) {
        return first.toString().length() <= threshold &&
                second.toString().length() <= threshold;
    }

    /**
     * Resolves base length of multiolier for algorithm
     *
     * @param first
     * @param second
     * @return
     */
    private int resolveBaseLen(BigInteger first, BigInteger second) {
        return Math.max(AlgorithmUtils.nextPowerOf2(first.toString().length()),
                AlgorithmUtils.nextPowerOf2(second.toString().length()));
    }

    /**
     * Add occurrence ov value (ad + bc) to statisctics
     *
     * @param adplusbc value of (ad + bc)
     */
    private void collectADplusBCStatistics(BigInteger adplusbc) {
        if (!collectADplusBCStatistics) {
            return;
        }

        if (ADplusBCStatistics == null) {
            ADplusBCStatistics = new HashMap<BigInteger, Integer>();
        }

        Integer value = ADplusBCStatistics.get(adplusbc);
        if (value == null) {
            value = 1;
        }
        else {
            value++;
        }
        ADplusBCStatistics.put(adplusbc, value);
    }

    /**
     * Clears ADplusBCStatistics
     */
    public void resetADplusBCStatistics() {
        if (ADplusBCStatistics != null) {
            ADplusBCStatistics.clear();
        }
    }

    /**
     * Returns the number of key occurrence in last multiplication
     *
     * @param key value of (ad + bc)
     * @return number of occurrence
     */
    public int getADplusBCStatistics(BigInteger key) {
        Integer value = ADplusBCStatistics.get(key);
        return value == null ? 0 : value.intValue();
    }

}
