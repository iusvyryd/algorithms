package iusvyryd.courses.kpi.week1;

import iusvyryd.courses.kpi.AbstractSortingAlgorithm;
import iusvyryd.courses.kpi.AlgorithmUtils;

/**
 * Insertion sort algorithm implementation
 *
 * Time complexity:
 * worst - n^2
 * average - n^2
 * best - n
 *
 * additional memory - 1
 * stable - true
 *
 */
public class InsertionSort<T extends Comparable<T>> extends AbstractSortingAlgorithm<T> {

    public InsertionSort() {
        super();
    }

    public InsertionSort(Direction direction) {
        super(direction);
    }

    @Override
    public T[] sort(T[] data) {
        if (data == null) {
            return null;
        }

        for (int i = 1; i < data.length; i++) {
            T key = data[i];
            int j = i - 1;
            while (j >= 0 && !isSorted(data[j], key)) {
                AlgorithmUtils.swap(data, j + 1, j);
                j--;
            }
            data[j+1] = key;
        }
        return data;
    }
}
