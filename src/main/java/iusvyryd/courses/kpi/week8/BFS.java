package iusvyryd.courses.kpi.week8;

import java.util.*;

/**
 * Breadth first search algorithm
 */
public class BFS<T> extends GraphSearchAlgorithm<T> {

    public BFS(Graph<T> graph, Graph.Node<T> node) {
        super(graph);
        bfs(graph, node);
    }

    private void bfs(Graph<T> graph, Graph.Node<T> start) {
        researched.put(start, 0);
        List<Graph.Node<T>> queue = new LinkedList<>();
        queue.add(start);
        while (!queue.isEmpty()) {
            Graph.Node a = queue.remove(0);
            int aRate = researched.get(a).intValue();
            Set<Graph.Node<T>> adjacentA = graph.getAdjacentNodes(a);
            for (Graph.Node<T> b : adjacentA) {
                if (!researched.containsKey(b)) {
                    researched.put(b, aRate + 1);
                    queue.add(b);
                }
            }
        }
    }
}
