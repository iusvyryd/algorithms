package iusvyryd.courses.kpi.week8;

import java.util.*;

/**
 * Tarjan's algorithm for finding strongly connected components in graph
 */
public class TarjansAlgorithm<T extends Comparable<T>> implements StronglyConnectedComponentsFinder<T> {

    private List<Graph.Node<T>> stack;
    private Set<Graph.Node<T>> researched;
    private Map<Graph.Node<T>, Integer> lowLevel;
    int time;
    private List<List<Graph.Node<T>>> components;

    @Override
    public List<List<Graph.Node<T>>> findStronglyConnectedComponents(Graph<T> graph) {
        int n = graph.getNodeCount();
        stack = new LinkedList<>();
        researched = new HashSet<>(n);
        lowLevel = new HashMap<>(n);
        time = 0;
        components = new LinkedList<>();

        Set<Graph.Node<T>> nodes = graph.getNodes();
        for (Graph.Node<T> node : nodes) {
            if (!researched.contains(node)) {
                dfs(graph, node);
            }
        }
        return components;
    }

    private void dfs(Graph<T> graph, Graph.Node<T> node) {
        lowLevel.put(node, time++);
        researched.add(node);
        stack.add(node);
        boolean rootComponent = true;

        Set<Graph.Node<T>> adjacent = graph.getAdjacentNodes(node);
        for (Graph.Node<T> adj : adjacent) {
            if (!researched.contains(adj)) {
                dfs(graph, adj);
            }
            if (lowLevel.get(node).compareTo(lowLevel.get(adj)) > 0) {
                lowLevel.put(node, lowLevel.get(adj));
                rootComponent = false;
            }
        }

        if (rootComponent) {
            List<Graph.Node<T>> component = new ArrayList<>();
            while (true) {
                Graph.Node<T> popped = stack.remove(stack.size() - 1);
                component.add(popped);
                lowLevel.put(popped, Integer.MAX_VALUE);
                if (popped.equals(node)) {
                    break;
                }
            }
            components.add(component);
        }
    }

}
