package iusvyryd.courses.kpi.week8;

import iusvyryd.courses.kpi.AlgorithmUtils;

import java.util.*;

public class Graph<T> {

    public static class Node<T> {
        public T data;
        public Node(T data) {
            this.data = data;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Node<?> node = (Node<?>) o;

            return !(data != null ? !data.equals(node.data) : node.data != null);
        }

        @Override
        public int hashCode() {
            return data != null ? data.hashCode() : 0;
        }

        @Override
        public String toString() {
            return data.toString();
        }
    }

    public enum Type {
        UNORIENTED,
        ORIENTED
    }

    private final Type type;
    private int edgesCount;
    private Map<Node<T>, Set<Node<T>>> adjacencyList;
    private Set<Node<T>> nodes;

    public Graph(Type type, int size) {
        this.type = type;
        adjacencyList = new HashMap<>(size);
        nodes = new HashSet<>(size);
    }

    private void add(Node<T> a, Node<T> b) {
        Set<Node<T>> edges = adjacencyList.get(a);
        if (edges == null) {
            edges = new LinkedHashSet<>();
        }
        edges.add(b);
        adjacencyList.put(a, edges);
    }

    public void addEdge(Node<T> a, Node<T> b) {
        if (!hasEdge(a, b)) {
            add(a, b);
            edgesCount++;
            nodes.add(a);
            nodes.add(b);
        }
        if (type == Type.UNORIENTED && !hasEdge(b, a)) {
            add(b, a);
        }
    }

    public Type getType() {
        return type;
    }

    public int getNodeCount() {
        return nodes.size();
    }

    public int getEdgesCount() {
        return edgesCount;
    }

    public Set<Node<T>> getAdjacentNodes(Node<T> a) {
        Set<Node<T>> edges = adjacencyList.get(a);
        return edges == null ? Collections.emptySet() : edges;
    }

    public boolean hasEdge(Node<T> a, Node<T> b) {
        Set<Node<T>> edges = getAdjacentNodes(a);
        return edges.contains(b);
    }

    public Set<Node<T>> getNodes() {
        return nodes;
    }

    public Graph<T> getTransformed() {
        if (type != Type.ORIENTED) {
            return null;
        }
        Graph<T> transformed = new Graph<>(Graph.Type.ORIENTED, nodes.size());
        for (Map.Entry<Graph.Node<T>, Set<Graph.Node<T>>> entry : adjacencyList.entrySet()) {
            Set<Graph.Node<T>> list = entry.getValue();
            for (Graph.Node<T> i : list) {
                transformed.addEdge(i, entry.getKey());
            }
        }
        return transformed;
    }

    public static <E> Graph<E> buildFromPairs(List<AlgorithmUtils.Pair<E, E>> pairs, Type type) {
        Graph<E> graph = new Graph<>(type, pairs.size());
        for (AlgorithmUtils.Pair<E, E> pair : pairs) {
            graph.addEdge(new Node<>(pair.first()), new Node<>(pair.second()));
        }
        return graph;
    }
}
