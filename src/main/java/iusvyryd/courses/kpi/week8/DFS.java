package iusvyryd.courses.kpi.week8;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Depth first search algorithm
 */
public class DFS<T> extends GraphSearchAlgorithm<T> {

    public DFS(Graph<T> graph, Graph.Node<T> node) {
        this(graph, node, false);
    }

    public DFS(Graph<T> graph, Graph.Node<T> node, boolean useRecursion) {
        super(graph);
        if (useRecursion) {
            dfsRecursive(graph, node, 0);
        }
        else {
            dfs(graph, node);
        }
    }

    private void dfsRecursive(Graph<T> graph, Graph.Node<T> a, int k) {
        researched.put(a, k);
        Set<Graph.Node<T>> adjacentA = graph.getAdjacentNodes(a);
        for (Graph.Node<T> b : adjacentA) {
            if (!researched.containsKey(b)) {
                dfsRecursive(graph, b, k + 1);
            }
        }
    }

    private void dfs(Graph<T> graph, Graph.Node<T> start) {
        researched.put(start, 0);
        List<Graph.Node<T>> stack = new LinkedList<>();
        stack.add(start);
        while (!stack.isEmpty()) {
            Graph.Node<T> a = stack.get(stack.size() - 1);
            int aRate = researched.get(a).intValue();
            Graph.Node<T> b = null;
            Set<Graph.Node<T>> adjacentA = graph.getAdjacentNodes(a);
            for (Graph.Node<T> adj : adjacentA) {
                if (!researched.containsKey(adj)) {
                    b = adj;
                    break;
                }
            }
            if (b != null) {
                researched.put(b, aRate + 1);
                stack.add(b);
            } else {
                stack.remove(stack.size() - 1);
            }
        }
    }
}
