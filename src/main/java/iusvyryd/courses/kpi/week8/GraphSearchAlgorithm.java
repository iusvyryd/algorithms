package iusvyryd.courses.kpi.week8;

import java.util.*;
import java.util.List;

public abstract class GraphSearchAlgorithm<T> {

    protected Map<Graph.Node<T>, Integer> researched;

    public GraphSearchAlgorithm(Graph<T> graph) {
        researched = new LinkedHashMap<>(graph.getNodeCount());
    }

    public boolean hasPathTo(Graph.Node<T> b) {
        return researched.containsKey(b);
    }

    public int getMinPathLength(Graph.Node<T> b) {
        return researched.get(b) == null ? -1 : researched.get(b).intValue();
    }

    public List<Graph.Node<T>> getPathMin(Graph.Node<T> b) {
        if (researched.get(b) == null) {
            return Collections.emptyList();
        }
        List<Graph.Node<T>> minPath = new LinkedList<>();
        for (Graph.Node<T> n : researched.keySet()) {
            minPath.add(n);
            if (n.equals(b)) {
                break;
            }
        }
        return minPath;
    }

    public List<Graph.Node<T>> getPathFull() {
        return new LinkedList<>(researched.keySet());
    }

}
