package iusvyryd.courses.kpi.week8;

import iusvyryd.courses.kpi.AlgorithmUtils;
import iusvyryd.courses.kpi.DataLoader;

import java.io.IOException;
import java.util.*;

/**
 * В даній роботі вам потрібно розв’язати задачу пошуку компонент сильної зв’язності в орієнтованому графі.
 *
 * На вхід подається орієнтований граф, який заданий текстовим файлом. Вершини цього графу позначені числами
 * від 1 до 875714. Кожний рядок у вхідному файлі відповідає орієнтованому ребру графа. Перше число рядка
 * вказує на початкову вершину ребра (з якої вершини ребро виходить) та друге число - кінцеву вершину ребра.
 * Наприклад, 9й рядок файлу містить запис "2 47646", який означає, що в графі є ребро, яке веде з вершини 2
 * у вершину 47646.
 *
 * Отже, вам потрібно визначити компоненти сильної зв’язності в цьому графі. Для цього ви можете скористатись
 * алгоритмом, який було наведено в лекції №13. Але будьте уважні, адже розмірність вхідного графу дуже сильно
 * може вплинути на якість роботи вашої програми.
 *
 * Вам потрібно ввести розмірність п’яти найбільших компонент сильної зв’язності для заданого вхідного графу.
 * Розмірність компоненти визначається кількістю вершин, які в неї входять. Ви повинні вводити розмірності за
 * зменшенням кількості вершин. Необхідно вказувати числа через один пробіл.
 *
 * Наприклад, якщо розмірності п’яти найбільших компонент становлять 500, 400, 300, 200 та 100, то ви повинні
 * ввести "500 400 300 200 100". Якщо компонент сильної зв’язності менше аніж п’ять, то вказуйте розмірності
 * тільки існуючих компонент (тоді кількість чисел буде менше п’яти).
 *
 */
public class PracticalTask8 {

    private StronglyConnectedComponentsFinder<Integer> finder;

    public PracticalTask8(StronglyConnectedComponentsFinder<Integer> finder) {
        this.finder = finder;
    }

    public List<Integer> getResult(Graph<Integer> graph) {
        List<List<Graph.Node<Integer>>> ssc = finder.findStronglyConnectedComponents(graph);
        List<Integer> sizes = new ArrayList<>();
        for (List<Graph.Node<Integer>> c : ssc) {
            sizes.add(c.size());
        }
        Collections.sort(sizes, Comparator.<Integer>reverseOrder());
        return sizes;
    }

    public static void main(String[] args) throws IOException {
        List<AlgorithmUtils.Pair<Integer, Integer>> pairs = DataLoader.loadIntegerPairs("week8/input_08.txt");
        Graph<Integer> graph = Graph.buildFromPairs(pairs, Graph.Type.ORIENTED);

        PracticalTask8 pt8 = new PracticalTask8(new TarjansAlgorithm<>());
        long begin = System.currentTimeMillis();
        List<Integer> sizes = pt8.getResult(graph);
        long end = System.currentTimeMillis();

        List<Integer> resultCut = sizes.subList(0, 5);
        System.out.println(resultCut);
        System.out.println(end - begin);
    }


}
