package iusvyryd.courses.kpi.week8;

import java.util.List;

public interface StronglyConnectedComponentsFinder<T> {

    List<List<Graph.Node<T>>> findStronglyConnectedComponents(Graph<T> graph);

}
