package iusvyryd.courses.kpi.week8;

import java.util.*;

/**
 * Algorithm for finding strongly connected components in graph
 */
public class StronglyConnectedComponentInOriented<T> implements StronglyConnectedComponentsFinder<T> {

    private Set<Graph.Node<T>> researched;
    private List<Graph.Node<T>> lowLevel;
    int time;
    private List<Graph.Node<T>> component;
    private List<List<Graph.Node<T>>> components;

    @Override
    public List<List<Graph.Node<T>>> findStronglyConnectedComponents(Graph<T> graph) {
        int n = graph.getNodeCount();
        researched = new HashSet<>(n);
        lowLevel = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            lowLevel.add(null);
        }
        time = 0;
        component = new ArrayList<>();
        components = new ArrayList<>();

        dfsLoop(graph);
        graph = graph.getTransformed();
        dfsLoop2(graph);
        return components;
    }

    private void dfsLoop(Graph<T> graph) {
        researched.clear();
        Set<Graph.Node<T>> nodes = graph.getNodes();
        for (Graph.Node<T> node : nodes) {
            if (!researched.contains(node)) {
                dfs(graph, node);
            }
        }
    }

    private void dfs(Graph<T> graph, Graph.Node<T> node) {
        researched.add(node);
        Set<Graph.Node<T>> adjacentA = graph.getAdjacentNodes(node);
        for (Graph.Node<T> adj : adjacentA) {
            if (!researched.contains(adj)) {
                dfs(graph, adj);
            }
        }
        lowLevel.set(time++, node);
    }

    private void dfsLoop2(Graph<T> graph) {
        researched.clear();
        for (int i = lowLevel.size() - 1; i >= 0 ; i--) {
            if (lowLevel.get(i) != null && !researched.contains(lowLevel.get(i))) {
                dfs2(graph, lowLevel.get(i));
                if (component.size() > 0) {
                    components.add(component);
                    component = new ArrayList<>();
                }
            }
        }
    }

    private void dfs2(Graph<T> graph, Graph.Node<T> node) {
        researched.add(node);
        Set<Graph.Node<T>> adjacentA = graph.getAdjacentNodes(node);
        for (Graph.Node<T> adj : adjacentA) {
            if (!researched.contains(adj)) {
                dfs2(graph, adj);
            }
        }
        component.add(node);
    }
}
