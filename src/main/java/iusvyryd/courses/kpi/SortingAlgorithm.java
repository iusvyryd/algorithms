package iusvyryd.courses.kpi;

/**
 * This interface describes the common sorting algorithm
 *
 * @param <T> the type of object in array to be sort
 */
public interface SortingAlgorithm<T extends Comparable<T>> {

    /**
     * Enumerates sorting directions
     *
     * ASC - ascending sorting direction
     * DESC - descending sorting direction
     */
    public enum Direction {
        ASC,
        DESC
    }

    /**
     * Sorts the specified array of objects
     *
     * @param data the array to be sort
     * @return sorted array that passed in parameter
     */
    public T[] sort(T[] data);

    /**
     * Returns the number of comparisons made at the last sorting
     * or 0 if there war no sorting or sorting algorithm not based on comparison
     *
     * @return number of comparisons
     */
    public int comparisonsCount();
}
