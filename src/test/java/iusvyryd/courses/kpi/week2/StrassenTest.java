package iusvyryd.courses.kpi.week2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StrassenTest {

    @Test(expected = IllegalArgumentException.class)
    public void testExcept() {
        int[][] a = {
                {1, 2},
                {3, 4}
        };
        int [][] b = {{1,2}};
        new Strassen(1).multiply(a, b);
    }

    @Test
    public void testMultiply() {
        int [][] a = {
                {1, 2, 3, 4},
                {5, 6, 7, 8}
        };
        int [][] b = {
                {1, 2},
                {3, 4},
                {5, 6},
                {7, 8}
        };
        int[][] c = new Strassen(1).multiply(a, b);
        assertEquals(50, c[0][0]);
        assertEquals(60, c[0][1]);
        assertEquals(114, c[1][0]);
        assertEquals(140, c[1][1]);
    }

    @Test
    public void testMultiply2() {
        int [][] a = {
                {1, 2, 3, 4},
                {5, 6, 7, 8}
        };
        int [][] b = {
                {1, 2},
                {3, 4},
                {5, 6},
                {7, 8}
        };
        int[][] c = new Strassen(1).multiply(b, a);
        assertEquals(11, c[0][0]);
        assertEquals(14, c[0][1]);
        assertEquals(17, c[0][2]);
        assertEquals(20, c[0][3]);
        assertEquals(23, c[1][0]);
        assertEquals(30, c[1][1]);
        assertEquals(37, c[1][2]);
        assertEquals(44, c[1][3]);
        assertEquals(35, c[2][0]);
        assertEquals(46, c[2][1]);
        assertEquals(57, c[2][2]);
        assertEquals(68, c[2][3]);
        assertEquals(47, c[3][0]);
        assertEquals(62, c[3][1]);
        assertEquals(77, c[3][2]);
        assertEquals(92, c[3][3]);
    }

}
