package iusvyryd.courses.kpi.week2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InversionsCounterTest {

    @Test
    public void testMaxForLength() {
        assertEquals(10, InversionsCounter.maxForLength(5));
    }

    @Test
    public void testDirectNull() {
        assertEquals(0, InversionsCounter.directCount(null));
        assertEquals(0, InversionsCounter.decompositionCount(null));
    }

    @Test
    public void testDirect() {
        Integer[] array = {4, 2, 8, 5, 3, 2, 1, 7, 9};
        assertEquals(16, InversionsCounter.directCount(array));
    }


    @Test
    public void testDecomposition() {
        Integer[] array = {4, 2, 8, 5, 3, 2, 1, 7, 9};
        assertEquals(16, InversionsCounter.decompositionCount(array));
    }

}
