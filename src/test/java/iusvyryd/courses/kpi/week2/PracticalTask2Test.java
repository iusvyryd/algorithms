package iusvyryd.courses.kpi.week2;

import iusvyryd.courses.kpi.DataLoader;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class PracticalTask2Test {

    @Test
    public void test5_5() throws IOException {
        PracticalTask2 pi = new PracticalTask2("week2/test_5_5.txt");
        assertEquals(8, pi.countInversion(0, 1));
        assertEquals(5, pi.countInversion(0, 2));
        assertEquals(2, pi.countInversion(0, 3));
        assertEquals(6, pi.countInversion(0, 4));

        assertEquals(2, pi.countInversion(3, 0));
        assertEquals(6, pi.countInversion(3, 1));
        assertEquals(5, pi.countInversion(3, 2));
        assertEquals(6, pi.countInversion(3, 4));
    }

    @Test
    public void test5_10() throws IOException {
        PracticalTask2 pi = new PracticalTask2("week2/test_5_10.txt");
        assertEquals(28, pi.countInversion(1, 0));
        assertEquals(18, pi.countInversion(1, 2));
        assertEquals(25, pi.countInversion(1, 3));
        assertEquals(23, pi.countInversion(1, 4));

        assertEquals(32, pi.countInversion(2, 0));
        assertEquals(18, pi.countInversion(2, 1));
        assertEquals(21, pi.countInversion(2, 3));
        assertEquals(19, pi.countInversion(2, 4));
    }

    @Test
    public void test10_5() throws IOException {
        PracticalTask2 pi = new PracticalTask2("week2/test_10_5.txt");
        assertEquals(6, pi.countInversion(6, 0));
        assertEquals(7, pi.countInversion(6, 1));
        assertEquals(2, pi.countInversion(6, 2));
        assertEquals(5, pi.countInversion(6, 3));
        assertEquals(7, pi.countInversion(6, 4));
        assertEquals(4, pi.countInversion(6, 5));
        assertEquals(2, pi.countInversion(6, 7));
        assertEquals(2, pi.countInversion(6, 8));
        assertEquals(8, pi.countInversion(6, 9));

        assertEquals(4, pi.countInversion(2, 0));
        assertEquals(5, pi.countInversion(2, 1));
        assertEquals(7, pi.countInversion(2, 3));
        assertEquals(5, pi.countInversion(2, 4));
        assertEquals(6, pi.countInversion(2, 5));
        assertEquals(2, pi.countInversion(2, 6));
        assertEquals(2, pi.countInversion(2, 7));
        assertEquals(4, pi.countInversion(2, 8));
        assertEquals(10, pi.countInversion(2, 9));
    }

    @Test
    public void test50_100() throws IOException {
        PracticalTask2 pi = new PracticalTask2("week2/test_50_100.txt");

        int[] for8 = DataLoader.loadIntArrayWithKnownZeroValue("week2/test_50_100_for_8.txt", 8, 50);
        assertArrayEquals(for8, pi.countInversionsAll(8));

        int[] for11 = DataLoader.loadIntArrayWithKnownZeroValue("week2/test_50_100_for_11.txt", 11, 50);
        assertArrayEquals(for11, pi.countInversionsAll(11));

        int[] for23 = DataLoader.loadIntArrayWithKnownZeroValue("week2/test_50_100_for_23.txt", 23, 50);
        assertArrayEquals(for23, pi.countInversionsAll(23));

        int[] for44 = DataLoader.loadIntArrayWithKnownZeroValue("week2/test_50_100_for_44.txt", 44, 50);
        assertArrayEquals(for44, pi.countInversionsAll(44));
    }

    @Test
    public void test100_50() throws IOException {
        PracticalTask2 pi = new PracticalTask2("week2/test_100_50.txt");

        int[] for14 = DataLoader.loadIntArrayWithKnownZeroValue("week2/test_100_50_for_14.txt", 14, 100);
        assertArrayEquals(for14, pi.countInversionsAll(14));

        int[] for20 = DataLoader.loadIntArrayWithKnownZeroValue("week2/test_100_50_for_20.txt", 20, 100);
        assertArrayEquals(for20, pi.countInversionsAll(20));

        int[] for47 = DataLoader.loadIntArrayWithKnownZeroValue("week2/test_100_50_for_47.txt", 47, 100);
        assertArrayEquals(for47, pi.countInversionsAll(47));

        int[] for59 = DataLoader.loadIntArrayWithKnownZeroValue("week2/test_100_50_for_59.txt", 59, 100);
        assertArrayEquals(for59, pi.countInversionsAll(59));
    }

}
