package iusvyryd.courses.kpi.week2;

import iusvyryd.courses.kpi.CommonSortingTest;
import iusvyryd.courses.kpi.SortingAlgorithm;
import org.junit.Before;
import org.junit.Test;

public class MergeSortTest extends CommonSortingTest {

    @Before
    public void setUp() {
        sa = new MergeSort<Integer>();
    }

    @Test
    public void testDesc() {
        sa = new MergeSort<Integer>(SortingAlgorithm.Direction.DESC);
        super.testDesc();
    }

}
