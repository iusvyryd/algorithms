package iusvyryd.courses.kpi.week8;

import iusvyryd.courses.kpi.AlgorithmUtils;
import iusvyryd.courses.kpi.DataLoader;
import org.junit.Test;

import java.io.IOException;
import java.util.*;

import static org.junit.Assert.assertEquals;

public class PracticalTask8Test {

    public void common(String in, String out) throws IOException {
        List<AlgorithmUtils.Pair<Integer, Integer>> pairs = DataLoader.loadIntegerPairs(in);
        int[] output = DataLoader.loadIntArrayFromSpacedOneLine(out);
        List<Integer> expected = new ArrayList<>();
        for (int i : output) {
            expected.add(i);
        }

        Graph<Integer> graph = Graph.buildFromPairs(pairs, Graph.Type.ORIENTED);

        PracticalTask8 pt8 = new PracticalTask8(new TarjansAlgorithm<>());
        List<Integer> result = pt8.getResult(graph);
        assertEquals(expected, result);
    }

    @Test
    public void test01() throws IOException {
        common("week8/test_08_1.txt", "week8/test_08_1.output.txt");
    }

    @Test
    public void test02() throws IOException {
        common("week8/test_08_2.txt", "week8/test_08_2.output.txt");
    }

    @Test
    public void test03() throws IOException {
        common("week8/test_08_3.txt", "week8/test_08_3.output.txt");
    }

    @Test
    public void test04() throws IOException {
        common("week8/test_08_4.txt", "week8/test_08_4.output.txt");
    }

}
