package iusvyryd.courses.kpi;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public abstract class CommonSortingTest {

    protected SortingAlgorithm<Integer> sa;

    @Test
    public void testNull() {
        Integer[] arrSorted = sa.sort(null);
        assertTrue(arrSorted == null);
    }

    @Test
    public void testSingle() {
        Integer[] array = {5};
        Integer[] arrSorted = sa.sort(array);

        assertTrue(arrSorted[0].intValue() == 5);
    }

    @Test
    public void testAsc() {
        Integer[] array = {4, 2, 8, 5, 3, 2, 1, 14, 7, 9};
        Integer[] arrExpected = {1, 2, 2, 3, 4, 5, 7, 8, 9, 14};

        Integer[] arrSorted = sa.sort(array);

        assertTrue(arrSorted[0].intValue() == arrExpected[0].intValue());
        assertTrue(arrSorted[1].intValue() == arrExpected[1].intValue());
        assertTrue(arrSorted[2].intValue() == arrExpected[2].intValue());
        assertTrue(arrSorted[3].intValue() == arrExpected[3].intValue());
        assertTrue(arrSorted[4].intValue() == arrExpected[4].intValue());
        assertTrue(arrSorted[5].intValue() == arrExpected[5].intValue());
        assertTrue(arrSorted[6].intValue() == arrExpected[6].intValue());
        assertTrue(arrSorted[7].intValue() == arrExpected[7].intValue());
        assertTrue(arrSorted[8].intValue() == arrExpected[8].intValue());
        assertTrue(arrSorted[9].intValue() == arrExpected[9].intValue());
    }

    protected void testDesc() {
        Integer[] array = {4, 2, 8, 5, 3, 2, 1, 14, 7, 9};
        Integer[] arrExpected = {14, 9, 8, 7, 5, 4, 3, 2, 2, 1};

        Integer[] arrSorted = sa.sort(array);

        assertTrue(arrSorted[0].intValue() == arrExpected[0].intValue());
        assertTrue(arrSorted[1].intValue() == arrExpected[1].intValue());
        assertTrue(arrSorted[2].intValue() == arrExpected[2].intValue());
        assertTrue(arrSorted[3].intValue() == arrExpected[3].intValue());
        assertTrue(arrSorted[4].intValue() == arrExpected[4].intValue());
        assertTrue(arrSorted[5].intValue() == arrExpected[5].intValue());
        assertTrue(arrSorted[6].intValue() == arrExpected[6].intValue());
        assertTrue(arrSorted[7].intValue() == arrExpected[7].intValue());
        assertTrue(arrSorted[8].intValue() == arrExpected[8].intValue());
        assertTrue(arrSorted[9].intValue() == arrExpected[9].intValue());
    }
}
