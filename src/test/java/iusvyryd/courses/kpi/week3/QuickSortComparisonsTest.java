package iusvyryd.courses.kpi.week3;

import iusvyryd.courses.kpi.DataLoader;
import iusvyryd.courses.kpi.SortingAlgorithm;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class QuickSortComparisonsTest {

    private QuickSort<Integer> qs0;
    private QuickSort<Integer> qs1;
    private QuickSort<Integer> qs2;

    @Before
    public void setUp() {
        qs0 = new QuickSort<Integer>(SortingAlgorithm.Direction.ASC,
                QuickSortBaseSelectionStrategies.Factory.strategy(
                        QuickSortBaseSelectionStrategies.StrategyType.LAST));
        qs1 = new QuickSort<Integer>(SortingAlgorithm.Direction.ASC,
                QuickSortBaseSelectionStrategies.Factory.strategy(
                        QuickSortBaseSelectionStrategies.StrategyType.FIRST));
        qs2 = new QuickSort<Integer>(SortingAlgorithm.Direction.ASC,
                QuickSortBaseSelectionStrategies.Factory.strategy(
                        QuickSortBaseSelectionStrategies.StrategyType.FIRST_MIDDLE_LAST_MEDIAN));
    }

    @Test
    public void testInput10() throws IOException {
        Integer[] arr0 = DataLoader.loadIntegerArray("week3/test_10.txt");
        Integer[] arr1 = Arrays.copyOf(arr0, arr0.length);
        Integer[] arr2 = Arrays.copyOf(arr0, arr0.length);

        qs0.sort(arr0);
        qs1.sort(arr1);
        qs2.sort(arr2);

        assertEquals(25, qs0.comparisonsCount());
        assertEquals(20, qs1.comparisonsCount());
        assertEquals(19, qs2.comparisonsCount());
    }

    @Test
    public void testInput100() throws IOException {
        Integer[] arr0 = DataLoader.loadIntegerArray("week3/test_100.txt");
        Integer[] arr1 = Arrays.copyOf(arr0, arr0.length);
        Integer[] arr2 = Arrays.copyOf(arr0, arr0.length);

        qs0.sort(arr0);
        qs1.sort(arr1);
        qs2.sort(arr2);

        assertEquals(656, qs0.comparisonsCount());
        assertEquals(582, qs1.comparisonsCount());
        assertEquals(586, qs2.comparisonsCount());
    }

    @Test
    public void testInput1000() throws IOException {
        Integer[] arr0 = DataLoader.loadIntegerArray("week3/test_1000.txt");
        Integer[] arr1 = Arrays.copyOf(arr0, arr0.length);
        Integer[] arr2 = Arrays.copyOf(arr0, arr0.length);

        qs0.sort(arr0);
        qs1.sort(arr1);
        qs2.sort(arr2);

        assertEquals(11027, qs0.comparisonsCount());
        assertEquals(11009, qs1.comparisonsCount());
        assertEquals(9290, qs2.comparisonsCount());
    }

}
