package iusvyryd.courses.kpi.week3;

import iusvyryd.courses.kpi.CommonSortingTest;
import iusvyryd.courses.kpi.SortingAlgorithm;
import org.junit.Before;
import org.junit.Test;

public class QuickSortTest extends CommonSortingTest {

    @Before
    public void setUp() {
        sa = new QuickSort<Integer>();
    }

    @Test
    public void testDesc() {
        sa = new QuickSort<Integer>(SortingAlgorithm.Direction.DESC);
        super.testDesc();
    }

}
