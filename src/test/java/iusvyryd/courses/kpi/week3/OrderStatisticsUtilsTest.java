package iusvyryd.courses.kpi.week3;

import iusvyryd.courses.kpi.AlgorithmUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OrderStatisticsUtilsTest {

    @Test
    public void testMinMax() {
        Integer[] arr = {4, 3, 6, 8, 1, 8, 9, 5, 0, 2, 7};
        AlgorithmUtils.Pair<Integer, Integer> minmax = OrderStatisticsUtils.minAndMax(arr);

        assertEquals(0, minmax.first().intValue());
        assertEquals(9, minmax.second().intValue());
    }

    @Test
    public void testOS() {
        Integer[] arr = {4, 3, 6, 8, 1, 8, 9, 5, 0, 2, 7};
        Integer os1 = OrderStatisticsUtils.findOrderStatistics(arr, 0);
        Integer osN = OrderStatisticsUtils.findOrderStatistics(arr, arr.length - 1);

        assertEquals(0, os1.intValue());
        assertEquals(9, osN.intValue());

        Integer os2 = OrderStatisticsUtils.findOrderStatistics(arr, 1);
        Integer os5 = OrderStatisticsUtils.findOrderStatistics(arr, 4);
        assertEquals(1, os2.intValue());
        assertEquals(4, os5.intValue());
    }

}
