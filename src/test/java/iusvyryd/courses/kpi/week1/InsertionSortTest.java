package iusvyryd.courses.kpi.week1;

import iusvyryd.courses.kpi.CommonSortingTest;
import iusvyryd.courses.kpi.SortingAlgorithm;
import org.junit.Before;
import org.junit.Test;

public class InsertionSortTest extends CommonSortingTest {

    @Before
    public void setUp() {
        sa = new InsertionSort<Integer>();
    }

    @Test
    public void testDesc() {
        sa = new InsertionSort<Integer>(SortingAlgorithm.Direction.DESC);
        super.testDesc();
    }

}
