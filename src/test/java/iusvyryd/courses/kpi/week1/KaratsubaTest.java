package iusvyryd.courses.kpi.week1;

import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

public class KaratsubaTest {

    private Karatsuba k;

    @Before
    public void setUp() {
        k = new Karatsuba(true, 1);
    }

    @Test
    public void test1() {
        BigInteger ret = k.multiply(new BigInteger("49823261"), new BigInteger("44269423"));
        assertEquals("test 1", new BigInteger("2205647016448403"), ret);
        assertEquals("test 1 stat 8", 5, k.getADplusBCStatistics(BigInteger.valueOf(8)));
        assertEquals("test 1 stat 7", 4, k.getADplusBCStatistics(BigInteger.valueOf(7)));
    }

    @Test
    public void test2() {
        BigInteger ret = k.multiply(new BigInteger("54761293"), new BigInteger("65394884"));
        assertEquals("test 2", new BigInteger("3581108403425012"), ret);
        assertEquals("test 2 stat 5", 4, k.getADplusBCStatistics(BigInteger.valueOf(5)));
    }

    @Test
    public void test3() {
        BigInteger ret = k.multiply(new BigInteger("9313685456934674"), new BigInteger("7658898761837539"));
        assertEquals("test 3", new BigInteger("71332574014261268360454523927286"), ret);
        assertEquals("test 3 stat 18", 5, k.getADplusBCStatistics(BigInteger.valueOf(18)));
        assertEquals("test 3 stat 9", 7, k.getADplusBCStatistics(BigInteger.valueOf(9)));
        assertEquals("test 3 stat 8", 8, k.getADplusBCStatistics(BigInteger.valueOf(8)));
    }

    @Test
    public void test4() {
        BigInteger ret = k.multiply(new BigInteger("3957322621234423"), new BigInteger("7748313756335578"));
        assertEquals("test 4", new BigInteger("30662577304368647842211393201494"), ret);
        assertEquals("test 4 stat 18", 5, k.getADplusBCStatistics(BigInteger.valueOf(14)));
        assertEquals("test 4 stat 9", 6, k.getADplusBCStatistics(BigInteger.valueOf(9)));
        assertEquals("test 4 stat 8", 13, k.getADplusBCStatistics(BigInteger.valueOf(8)));
    }

    @Test
    public void test5() {
        BigInteger ret = k.multiply(new BigInteger("34215432964249374812219364786397"), new BigInteger("94541964835273822784327848699719"));
        assertEquals("test 5", new BigInteger("3234794260129733170788831535430575611379062580407060392628922443"), ret);
        assertEquals("test 5 stat 45", 11, k.getADplusBCStatistics(BigInteger.valueOf(45)));
        assertEquals("test 5 stat 9", 28, k.getADplusBCStatistics(BigInteger.valueOf(9)));
        assertEquals("test 5 stat 8", 17, k.getADplusBCStatistics(BigInteger.valueOf(8)));
    }

    @Test
    public void test6() {
        BigInteger ret = k.multiply(new BigInteger("71611955325935479159397713213124"),
                new BigInteger("93567726499788166681348352945366"));
        assertEquals("test 6", new BigInteger("6700567850052179472481148730882040129649508491917840721086183384"), ret);
        assertEquals("test 6 stat 40", 15, k.getADplusBCStatistics(BigInteger.valueOf(40)));
        assertEquals("test 6 stat 36", 11, k.getADplusBCStatistics(BigInteger.valueOf(36)));
        assertEquals("test 6 stat 10", 11, k.getADplusBCStatistics(BigInteger.valueOf(10)));
    }

    @Test
    public void test7() {
        BigInteger ret = k.multiply(new BigInteger("8436939677358274975644341226884162349535836199962392872868456892"),
                new BigInteger("3864264464372346883776335161325428226997417338413342945574123327"));
        assertEquals("test 7", new BigInteger("32602566183268675582196165592691544162522778809155901895617284287276672563976841699892789718741377833554298336397153444191119684"), ret);
        assertEquals("test 7 stat 72", 14, k.getADplusBCStatistics(BigInteger.valueOf(72)));
        assertEquals("test 7 stat 69", 12, k.getADplusBCStatistics(BigInteger.valueOf(69)));
        assertEquals("test 7 stat 67", 14, k.getADplusBCStatistics(BigInteger.valueOf(67)));
    }

    @Test
    public void test8() {
        BigInteger ret = k.multiply(new BigInteger("8711129198194917883527844183686122989894424943636426448417394566"),
                new BigInteger("2924825637132661199799711722273977411715641477832758942277358764"));
        assertEquals("test 8", new BigInteger("25478534007255378799894857247961445544397925869179138904636157575535921570058983065006369481295619500406669960288667484926076424"), ret);
        assertEquals("test 8 stat 64", 20, k.getADplusBCStatistics(BigInteger.valueOf(64)));
        assertEquals("test 8 stat 60", 12, k.getADplusBCStatistics(BigInteger.valueOf(60)));
        assertEquals("test 8 stat 58", 12, k.getADplusBCStatistics(BigInteger.valueOf(58)));
    }

}
