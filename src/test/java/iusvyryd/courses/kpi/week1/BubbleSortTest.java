package iusvyryd.courses.kpi.week1;

import iusvyryd.courses.kpi.CommonSortingTest;
import iusvyryd.courses.kpi.SortingAlgorithm;
import org.junit.Before;
import org.junit.Test;

public class BubbleSortTest extends CommonSortingTest {

    @Before
    public void setUp() {
        sa = new BubbleSort<Integer>();
    }

    @Test
    public void testDesc() {
        sa = new BubbleSort<Integer>(SortingAlgorithm.Direction.DESC);
        super.testDesc();
    }
}
