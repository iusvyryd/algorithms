package iusvyryd.courses.kpi.week4;

import iusvyryd.courses.kpi.CommonSortingTest;
import iusvyryd.courses.kpi.SortingAlgorithm;
import org.junit.Before;
import org.junit.Test;

public class CountingSortTest extends CommonSortingTest {

    @Before
    public void setUp() {
        sa = new CountingSort();
    }

    @Test
    public void testDesc() {
        sa = new CountingSort(SortingAlgorithm.Direction.DESC);
        super.testDesc();
    }

}
