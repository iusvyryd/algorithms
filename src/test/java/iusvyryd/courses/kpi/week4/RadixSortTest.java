package iusvyryd.courses.kpi.week4;

import iusvyryd.courses.kpi.CommonSortingTest;
import iusvyryd.courses.kpi.SortingAlgorithm;
import org.junit.Before;
import org.junit.Test;

public class RadixSortTest extends CommonSortingTest{

    @Before
    public void setUp() {
        sa = new RadixSort();
    }

    @Test
    public void testDesc() {
        sa = new RadixSort(SortingAlgorithm.Direction.DESC);
        super.testDesc();
    }

}
