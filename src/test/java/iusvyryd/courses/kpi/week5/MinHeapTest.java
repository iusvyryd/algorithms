package iusvyryd.courses.kpi.week5;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MinHeapTest {

    MinHeap mh;

    @Before
    public void setUp() {
        Integer[] arr = {4,1,3,2,16,9,10,14,8,7};
        mh = new MinHeap(arr);
    }

    @Test
    public void testHeap() {
        String expected = ": [1, 2, 3, 4, 7, 9, 10, 14, 8, 16]";
        assertEquals(expected, mh.toString());
    }

    @Test
    public void testInsert() {
        mh.insert(0);
        String expected = ": [0, 1, 3, 4, 2, 9, 10, 14, 8, 16, 7]";
        assertEquals(expected, mh.toString());
    }

    @Test
    public void testExtract() {
        Integer min = (Integer)mh.extractMin();
        assertEquals(1, min.intValue());
        String expected = ": [2, 4, 3, 8, 7, 9, 10, 14, 16]";
        assertEquals(expected, mh.toString());
    }

    @Test(expected = IllegalStateException.class)
    public void testThrow() {
        Integer[] arr = {4};
        mh.build(arr);
    }
}
