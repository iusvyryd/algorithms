package iusvyryd.courses.kpi.week5;

import iusvyryd.courses.kpi.CommonSortingTest;
import iusvyryd.courses.kpi.SortingAlgorithm;
import org.junit.Before;
import org.junit.Test;

public class HeapSortTest extends CommonSortingTest {

    @Before
    public void setUp() {
        sa = new HeapSort<>();
    }

    @Test
    public void testDesc() {
        sa = new HeapSort<>(SortingAlgorithm.Direction.DESC);
        super.testDesc();
    }

}
