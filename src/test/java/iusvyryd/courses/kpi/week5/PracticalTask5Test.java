package iusvyryd.courses.kpi.week5;

import iusvyryd.courses.kpi.AlgorithmUtils;
import iusvyryd.courses.kpi.DataLoader;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class PracticalTask5Test {

    private void commonTest(String data, String expect, int size) throws IOException {
        PracticalTask5 pt = new PracticalTask5(data);
        Map<Integer, AlgorithmUtils.Triple<String, String, String>> expected =
                DataLoader.loadStringTripple(expect);
        for (int i = 1; i <= size; i++) {
            StringBuilder sb = new StringBuilder("Medians: ");
            sb.append(pt.medians.get(i).first());
            if (pt.medians.get(i).second() != null) {
                sb.append(" ").append(pt.medians.get(i).second());
            }
            assertEquals(sb.toString(), expected.get(i).first());
            assertEquals(pt.heaps.get(i).first(), expected.get(i).second());
            assertEquals(pt.heaps.get(i).second(), expected.get(i).third());
        }
    }

    @Test
    public void test1() throws IOException {
        commonTest("week5/input_01_10.txt", "week5/ext_output_01_10.txt", 10);
    }

    @Test
    public void test2() throws IOException {
        commonTest("week5/input_02_10.txt", "week5/ext_output_02_10.txt", 10);
    }

    @Test
    public void test3() throws IOException {
        commonTest("week5/input_03_10.txt", "week5/ext_output_03_10.txt", 10);
    }

    @Test
    public void test4() throws IOException {
        commonTest("week5/input_04_10.txt", "week5/ext_output_04_10.txt", 10);
    }

    @Test
    public void test5() throws IOException {
        commonTest("week5/input_05_100.txt", "week5/ext_output_05_100.txt", 100);
    }

    @Test
    public void test6() throws IOException {
        commonTest("week5/input_06_100.txt", "week5/ext_output_06_100.txt", 100);
    }

    @Test
    public void test7() throws IOException {
        commonTest("week5/input_07_100.txt", "week5/ext_output_07_100.txt", 100);
    }

    @Test
    public void test8() throws IOException {
        commonTest("week5/input_08_100.txt", "week5/ext_output_08_100.txt", 100);
    }

    @Test
    public void test9() throws IOException {
        commonTest("week5/input_09_1000.txt", "week5/ext_output_09_1000.txt", 1000);
    }

    @Test
    public void test10() throws IOException {
        commonTest("week5/input_10_1000.txt", "week5/ext_output_10_1000.txt", 1000);
    }

    @Test
    public void test11() throws IOException {
        commonTest("week5/input_11_1000.txt", "week5/ext_output_11_1000.txt", 1000);
    }

    @Test
    public void test12() throws IOException {
        commonTest("week5/input_12_1000.txt", "week5/ext_output_12_1000.txt", 1000);
    }
}
