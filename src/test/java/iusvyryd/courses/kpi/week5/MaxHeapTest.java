package iusvyryd.courses.kpi.week5;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MaxHeapTest {

    MaxHeap mh;

    @Before
    public void setUp() {
        Integer[] arr = {4,1,3,2,16,9,10,14,8,7};
        mh = new MaxHeap(arr);
    }

    @Test
    public void testHeap() {
        String expected = ": [16, 14, 10, 8, 7, 9, 3, 2, 4, 1]";
        assertEquals(expected, mh.toString());
    }

    @Test
    public void testInsert() {
        mh.insert(0);
        String expected = ": [16, 14, 10, 8, 7, 9, 3, 2, 4, 1, 0]";
        assertEquals(expected, mh.toString());

        mh.insert(11);
        expected = ": [16, 14, 11, 8, 7, 10, 3, 2, 4, 1, 0, 9]";
        assertEquals(expected, mh.toString());
    }

    @Test
    public void testExtract() {
        Integer max = (Integer)mh.extractMax();
        assertEquals(16, max.intValue());
        String expected = ": [14, 8, 10, 4, 7, 9, 3, 2, 1]";
        assertEquals(expected, mh.toString());
    }

    @Test(expected = IllegalStateException.class)
    public void testThrow() {
        Integer[] arr = {4};
        mh.build(arr);
    }

}
