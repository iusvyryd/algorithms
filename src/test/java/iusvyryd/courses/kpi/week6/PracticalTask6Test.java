package iusvyryd.courses.kpi.week6;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class PracticalTask6Test {

    @Test
    public void testTask() throws IOException {
        PracticalTask6 pt = new PracticalTask6(100000, "week6/test_06.txt");
        assertEquals(22, pt.getResult());
    }

}
