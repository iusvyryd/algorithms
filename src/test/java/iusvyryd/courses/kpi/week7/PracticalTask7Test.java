package iusvyryd.courses.kpi.week7;

import iusvyryd.courses.kpi.DataLoader;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PracticalTask7Test {

    @Test
    public void testA() throws IOException {
        int[] arr = DataLoader.loadIntArrayFromSpacedOneLine("week7/input_10a.txt");
        PracticalTask7 pt = new PracticalTask7();
        pt.treeFromArray(arr);

        int[] fromTree = pt.toInorderArray();
        Arrays.sort(fromTree);
        pt.transformToBST(fromTree);

        List<List<Integer>> ll = pt.monoPaths(9);
        assertTrue(ll.size() == 3);
        Integer[] exp0 = {4, 5};
        Integer[] exp1 = {9};
        Integer[] exp2 = {6, 3};
        for (List<Integer> li : ll) {
            Integer[] liarr = li.toArray(new Integer[li.size()]);
            assertTrue(Arrays.equals(liarr, exp0)
                    || Arrays.equals(liarr, exp1)
                    || Arrays.equals(liarr, exp2));
        }

    }

    @Test
    public void testB() throws IOException {
        int[] arr = DataLoader.loadIntArrayFromSpacedOneLine("week7/input_10b.txt");
        PracticalTask7 pt = new PracticalTask7();
        pt.treeFromArray(arr);

        int[] fromTree = pt.toInorderArray();
        Arrays.sort(fromTree);
        pt.transformToBST(fromTree);

        List<List<Integer>> ll = pt.monoPaths(7);
        assertTrue(ll.size() == 3);
        Integer[] exp0 = {3, 4};
        Integer[] exp1 = {7};
        Integer[] exp2 = {5, 2};
        for (List<Integer> li : ll) {
            Integer[] liarr = li.toArray(new Integer[li.size()]);
            assertTrue(Arrays.equals(liarr, exp0)
                    || Arrays.equals(liarr, exp1)
                    || Arrays.equals(liarr, exp2));
        }
    }

    @Test
    public void testC() throws IOException {
        int[] arr = DataLoader.loadIntArrayFromSpacedOneLine("week7/input_10c.txt");
        PracticalTask7 pt = new PracticalTask7();
        pt.treeFromArray(arr);

        int[] fromTree = pt.toInorderArray();
        Arrays.sort(fromTree);
        pt.transformToBST(fromTree);

        List<List<Integer>> ll = pt.monoPaths(9);
        assertTrue(ll.size() == 3);
        Integer[] exp0 = {4, 2, 3};
        Integer[] exp1 = {9};
        Integer[] exp2 = {5, 4};
        for (List<Integer> li : ll) {
            Integer[] liarr = li.toArray(new Integer[li.size()]);
            assertTrue(Arrays.equals(liarr, exp0)
                    || Arrays.equals(liarr, exp1)
                    || Arrays.equals(liarr, exp2));
        }
    }

    @Test
    public void testD() throws IOException {
        int[] arr = DataLoader.loadIntArrayFromSpacedOneLine("week7/input_10d.txt");
        PracticalTask7 pt = new PracticalTask7();
        pt.treeFromArray(arr);

        int[] fromTree = pt.toInorderArray();
        Arrays.sort(fromTree);
        pt.transformToBST(fromTree);

        List<List<Integer>> ll = pt.monoPaths(5);
        assertTrue(ll.size() == 1);
        Integer[] exp0 = {5};
        for (List<Integer> li : ll) {
            Integer[] liarr = li.toArray(new Integer[li.size()]);
            assertTrue(Arrays.equals(liarr, exp0));
        }
    }

    @Test
    public void testE() throws IOException {
        int[] arr = DataLoader.loadIntArrayFromSpacedOneLine("week7/input_10c.txt");
        PracticalTask7 pt = new PracticalTask7();
        pt.treeFromArray(arr);

        int[] fromTree = pt.toInorderArray();
        Arrays.sort(fromTree);
        pt.transformToBST(fromTree);

        List<List<Integer>> ll = pt.monoPaths(5);
        assertTrue(ll.size() == 2);
        Integer[] exp0 = {5};
        Integer[] exp1 = {2, 3};
        for (List<Integer> li : ll) {
            Integer[] liarr = li.toArray(new Integer[li.size()]);
            assertTrue(Arrays.equals(liarr, exp0)
                    || Arrays.equals(liarr, exp1));
        }
    }

    @Test
    public void test100A() throws IOException {
        int[] arr = DataLoader.loadIntArrayFromSpacedOneLine("week7/input_100a.txt");
        PracticalTask7 pt = new PracticalTask7();
        pt.treeFromArray(arr);

        int[] fromTree = pt.toInorderArray();
        Arrays.sort(fromTree);
        pt.transformToBST(fromTree);

        List<List<Integer>> ll = pt.monoPaths(51);
        assertTrue(ll.size() == 4);
        Integer[] exp0 = {26, 25};
        Integer[] exp1 = {11, 15, 13, 12};
        Integer[] exp2 = {51};
        Integer[] exp3 = {27, 11, 7, 3, 1, 2};
        for (List<Integer> li : ll) {
            Integer[] liarr = li.toArray(new Integer[li.size()]);
            assertTrue(Arrays.equals(liarr, exp0)
                    || Arrays.equals(liarr, exp1)
                    || Arrays.equals(liarr, exp2)
                    || Arrays.equals(liarr, exp3));
        }
    }

    @Test
    public void test100B() throws IOException {
        int[] arr = DataLoader.loadIntArrayFromSpacedOneLine("week7/input_100b.txt");
        PracticalTask7 pt = new PracticalTask7();
        pt.treeFromArray(arr);

        int[] fromTree = pt.toInorderArray();
        Arrays.sort(fromTree);
        pt.transformToBST(fromTree);

        List<List<Integer>> ll = pt.monoPaths(78);
        assertTrue(ll.size() == 3);
        Integer[] exp0 = {24, 26, 28};
        Integer[] exp1 = {29, 14, 5, 9, 8, 7, 6};
        Integer[] exp2 = {78};
        for (List<Integer> li : ll) {
            Integer[] liarr = li.toArray(new Integer[li.size()]);
            assertTrue(Arrays.equals(liarr, exp0)
                    || Arrays.equals(liarr, exp1)
                    || Arrays.equals(liarr, exp2));
        }
    }

    @Test
    public void test100C() throws IOException {
        int[] arr = DataLoader.loadIntArrayFromSpacedOneLine("week7/input_100c.txt");
        PracticalTask7 pt = new PracticalTask7();
        pt.treeFromArray(arr);

        int[] fromTree = pt.toInorderArray();
        Arrays.sort(fromTree);
        pt.transformToBST(fromTree);

        List<List<Integer>> ll = pt.monoPaths(103);
        assertTrue(ll.size() == 4);
        Integer[] exp0 = {32, 35, 36};
        Integer[] exp1 = {25, 13, 20, 16, 15, 14};
        Integer[] exp2 = {25, 13, 20, 23, 22};
        Integer[] exp3 = {51, 25, 13, 6, 4, 1, 3};
        for (List<Integer> li : ll) {
            Integer[] liarr = li.toArray(new Integer[li.size()]);
            assertTrue(Arrays.equals(liarr, exp0)
                    || Arrays.equals(liarr, exp1)
                    || Arrays.equals(liarr, exp2)
                    || Arrays.equals(liarr, exp3));
        }
    }

    @Test
    public void test100D() throws IOException {
        int[] arr = DataLoader.loadIntArrayFromSpacedOneLine("week7/input_100d.txt");
        PracticalTask7 pt = new PracticalTask7();
        pt.treeFromArray(arr);

        int[] fromTree = pt.toInorderArray();
        Arrays.sort(fromTree);
        pt.transformToBST(fromTree);

        List<List<Integer>> ll = pt.monoPaths(50);
        assertTrue(ll.size() == 3);
        Integer[] exp0 = {14, 5, 8, 11, 12};
        Integer[] exp1 = {28, 14, 5, 3};
        Integer[] exp2 = {50};
        for (List<Integer> li : ll) {
            Integer[] liarr = li.toArray(new Integer[li.size()]);
            assertTrue(Arrays.equals(liarr, exp0)
                    || Arrays.equals(liarr, exp1)
                    || Arrays.equals(liarr, exp2));
        }
    }

    @Test
    public void test100E() throws IOException {
        int[] arr = DataLoader.loadIntArrayFromSpacedOneLine("week7/input_100e.txt");
        PracticalTask7 pt = new PracticalTask7();
        pt.treeFromArray(arr);

        int[] fromTree = pt.toInorderArray();
        Arrays.sort(fromTree);
        pt.transformToBST(fromTree);

        List<List<Integer>> ll = pt.monoPaths(50);
        assertTrue(ll.size() == 2);
        Integer[] exp0 = {18, 9, 13, 10};
        Integer[] exp1 = {50};
        for (List<Integer> li : ll) {
            Integer[] liarr = li.toArray(new Integer[li.size()]);
            assertTrue(Arrays.equals(liarr, exp0)
                    || Arrays.equals(liarr, exp1));
        }
    }
}
